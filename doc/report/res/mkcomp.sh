#!/bin/bash
# $File: mkcomp.sh
# $Date: Sun May 06 09:00:37 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in demo-*.jpg
do
	echo $i
	convert $i -fill yellow -pointsize 25  -annotate +0+20 @${i%.jpg}.log comp-$i
done
