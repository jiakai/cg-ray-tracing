/*
 * $File: scene.cc
 * $Date: Thu Jun 07 20:08:54 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "scene.hh"
#include "utils.hh"
#include "lib/thread.hh"
#include "debug.hh"

#include <cassert>
#include <list>
#include <limits>

using IllumMode = PrimitiveBase::Material::IllumMode;

struct Scene::StatData
{
	int max_dep = 0, nray = 0, call_cnt = 0;
	thread::MutexLock lock;
};

struct Scene::DataList
{
	std::list< RefCntPtr<PrimitiveBase>> prmtv;
	std::list< PointLightSource > pls;
};

Scene::Scene(double min_weight, double ambient_factor, int max_depth):
	m_data_list(new DataList),
	m_min_weight(min_weight), m_ambient_factor(ambient_factor),
	m_depth_limit(max_depth),
	m_stat(new StatData)
{
}

Scene::~Scene()
{
	delete m_data_list;
	delete m_stat;
}

int Scene::get_max_recursive_depth() const
{
	return m_stat->max_dep + 1;
}

double Scene::get_avg_recursive_depth() const
{
	return m_stat->call_cnt / double(m_stat->nray);
}

void Scene::add_primitive(const RefCntPtr<PrimitiveBase> &ptr)
{
	m_data_list->prmtv.push_back(ptr);
}

void Scene::add_point_light_source(const PointLightSource &pls)
{
	m_data_list->pls.push_back(pls);
}

void Scene::ray_tracing_init()
{
	m_ambient = Color(0, 0, 0);
	sassert(m_data_list->pls.size());
	sassert(m_min_weight >= EPS);
	for (const auto &i: m_data_list->pls)
		m_ambient += i.color * i.weight;
	m_ambient *= m_ambient_factor;
}

Color Scene::ray_tracing(const Ray &ray) const
{
	m_stat->lock.enter();
	m_stat->nray ++;
	m_stat->lock.exit();
	return do_ray_tracing(ray, 1, 0);
}

Color Scene::do_ray_tracing(const Ray &ray, double weight, int depth) const
{
	if (depth > m_depth_limit)
		return Color::BLACK;

	m_stat->lock.enter();
	update_max(m_stat->max_dep, depth);
	m_stat->call_cnt ++;
	m_stat->lock.exit();

	assert(fabs(ray.dir.mod_sqr() - 1) < EPS);
	if (weight < m_min_weight)
		return Color::BLACK;

	PrimitiveBase::RayTask* itrt = find_intersection(ray);
	if (!itrt)
		return Color::BLACK;

	Vector norm = itrt->get_normal(),
		   itsc_point = ray.get_point(itrt->get_intersection());
	AutoPtr<const OpticalProperty> optp;
	itrt->get_opt_prpt(optp);


#ifdef CONFIG_DEBUG_LOG_RAY
	static int call_cnt = 0;
	debug_mesh_out_line_segment(ssprintf("ray tracing.%d.ray", call_cnt ++),
			ray.src, itsc_point);
#endif	// CONFIG_DEBUG_LOG_RAY


	assert(fabs(norm.mod_sqr() - 1) < EPS);

	bool has_diffuse = !optp->diffuse.is_black(),
		 has_ambient = optp->ambient > EPS,
		 has_specular = optp->specular > EPS;

	Color ret;

	// Phong model
	if (has_ambient || has_diffuse)
	{
		for (const auto &i: m_data_list->pls)
		{
			Vector li = i.pos - itsc_point;
			real_t li_mod = li.mod();
			Vector li_norm(li);
			li_norm *= 1 / li_mod;

			if (test_shadow(Ray(itsc_point, li_norm), li_mod))
				continue;

			// diffuse
			if (has_diffuse)
			{
				real_t tmp = li_norm.dot(norm);
				if (tmp > 0)
					ret += i.color * (optp->diffuse * (tmp * i.weight));
			}

			// specular
			if (has_specular)
			{
				real_t tmp = norm.get_reflection(li_norm).dot(-ray.dir);
				if (tmp > 0)
					ret += i.color * (optp->specular * (
						(pow(tmp, optp->shininess) * i.weight)));
			}
		}
		// ambient
		if (has_ambient)
			ret += m_ambient * optp->ambient;
	}

	// reflection
	if (has_specular && 
			(itrt->get_primitive().get_material().get_illum_mode() &
			  IllumMode::REFLECTION))
	{
		ret += do_ray_tracing(Ray(itsc_point,
					-norm.get_reflection(ray.dir)), weight * optp->specular, depth + 1) * optp->specular;
	}

	// transmission
	if (optp->transparency > EPS)
	{
		Ray trans;
		itrt->get_transmission(trans);
		ret += do_ray_tracing(trans,
				weight * optp->transparency, depth + 1) * optp->transparency;
	}

	delete itrt;
	ret.set_to_range();
	return ret;
}

PrimitiveBase::RayTask* Scene::find_intersection(const Ray &ray) const
{
	auto min_dist = std::numeric_limits<real_t>::max();
	PrimitiveBase::RayTask* rt = nullptr, *tmp;
	for (const auto &i: m_data_list->prmtv)
	{
		tmp = i->make_ray_task(ray);
		if (tmp)
		{
			auto d = tmp->get_intersection();
			if (d < min_dist)
			{
				min_dist = d;
				delete rt;
				rt = tmp;
			} else delete tmp;
		}
	}
	return rt;
}

static bool ray_test_visible_req_shadow(const Ray &ray, const PrimitiveBase &prmtv)
{
	return prmtv.get_material().get_illum_mode() & IllumMode::SHADOW;
}

bool Scene::test_shadow(const Ray &ray0, real_t max_dist) const
{
#ifdef CONFIG_DEBUG_NO_SHADOW
	return false;
#endif
	Ray ray(ray0);
	ray.test_visible = ray_test_visible_req_shadow;
	for (const auto &i: m_data_list->prmtv)
	{
		auto tmp = i->make_ray_task(ray);
		if (tmp)
		{
			if (tmp->get_intersection() < max_dist)
			{
				delete tmp;
				return true;
			}
			delete tmp;
		}
	}
	return false;
}

// vim: syntax=cpp11.doxygen

