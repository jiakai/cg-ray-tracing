/*
 * $File: bsptree.cc
 * $Date: Thu Jun 07 20:01:45 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "bsptree.hh"
#include "utils.hh"
#include "debug.hh"

#include <alloca.h>

#include <cstring>

#include <vector>
#include <algorithm>
#include <limits>

struct BSPTree::Node
{
	AABox box;
	Node *chl = nullptr, *chr = nullptr;

	typedef std::vector<RefCntPtr<BSPTree::Primitive>> prmtv_list_t;
	prmtv_list_t* prmtv_list;
	// only for leaves, prmtv_list != NULL



	Node(const AABox &box_):
		box(box_), prmtv_list(new prmtv_list_t)
	{ }

	void split();

	/*!
	 * return INF if prmtv_list is NULL
	 */
	int get_nprmtv()
	{ return prmtv_list ? prmtv_list->size() : 
		std::numeric_limits<int>::max(); }

	~Node()
	{ delete chl; delete chr; delete prmtv_list; }

};

struct BSPTree::StatData
{
	int max_dep = 0, tot_dep = 0, nleaf = 0,
		max_lsize = 0, tot_lsize = 0;

	void update(Node *root, int dep)
	{
		if (root->prmtv_list)
		{
			update_max(max_dep, dep);
			tot_dep += dep;
			nleaf ++;
			int s = root->prmtv_list->size();
			update_max(max_lsize, s);
			tot_lsize += s;
			return;
		}
		update(root->chl, dep + 1);
		update(root->chr, dep + 1);
	}
};


BSPTree::BSPTree(const AABox &box, const Config &conf):
	m_conf(conf), m_root(new Node(box))
{
}

BSPTree::~BSPTree()
{
	StatData stat;
	stat.update(m_root, 1);
	print_debug("max_dep=%d avg_dep=%.3lf max_lsize=%d avg_lsize=%.3lf",
			stat.max_dep, double(stat.tot_dep) / stat.nleaf,
			stat.max_lsize, double(stat.tot_lsize) / stat.nleaf);
	delete m_root;
}

void BSPTree::add_primitive(const RefCntPtr<Primitive>& primitive)
{
	m_prmtv_user_data_len = std::max(m_prmtv_user_data_len,
			primitive->get_user_data_size());
	do_add_primitive(m_root, primitive, m_conf.max_depth - 1);
}

void BSPTree::do_add_primitive(Node *root, const RefCntPtr<Primitive>& primitive, int depth)
{
	if (!depth || root->get_nprmtv() < m_conf.inode_max_size ||
			Vector(root->box.get_v0(), root->box.get_v1()).get_min_comp_abs() < m_conf.min_box_size)
	{
		root->prmtv_list->push_back(primitive);
		return;
	}

	if (root->prmtv_list)
	{
		root->split();
		auto list = root->prmtv_list;
		root->prmtv_list = nullptr;
		for (auto &i: *list)
			do_add_primitive(root, i, depth);
		delete list;
	}
	if (primitive->test_intersect(root->chl->box))
		do_add_primitive(root->chl, primitive, depth - 1);
	if (primitive->test_intersect(root->chr->box))
		do_add_primitive(root->chr, primitive, depth - 1);
}

void BSPTree::Node::split()
{
	const Vector
		&v0(box.get_v0()),
		&v1(box.get_v1());

	real_t cur_max = -1;
	Vector delta(v0, v1), dir;

	sassert(delta.x > EPS && delta.y > EPS && delta.z > EPS);

	for (int c = 0; c < 3; c ++)
		if (update_max(cur_max, delta.get_comp(c)))
		{
			dir.x = dir.y = dir.z = 0;
			dir.get_comp(c) = delta.get_comp(c) / 2;
		}

	chl = new Node(AABox(v0, v0 + delta - dir));
	chr = new Node(AABox(v0 + dir, v1));
}

PrimitiveBase::RayTask* BSPTree::make_ray_task(const Ray &ray)
{
#ifdef CONFIG_DEBUG_BSPT_TRACEROUTE
	static int call_cnt = 0;
	debug_mesh_out_line_segment(ssprintf("BSPTree::make_ray_task.ray.%d",
				call_cnt ++), ray.src, ray.get_point(100));
#endif
	return do_make_ray_task(m_root, ray);
}

PrimitiveBase::RayTask*
	BSPTree::do_make_ray_task(Node *root, const Ray &ray)
{
#ifdef CONFIG_DEBUG_BSPT_TRACEROUTE
	static int call_cnt;
	auto mesh = new_debug_mesh_out(ssprintf("BSPTree.0x%d.ray_task.%d", (int)this, call_cnt ++));
	root->box.dump_mesh(*mesh);
	delete mesh;
#endif
	if (root->prmtv_list)
	{
		real_t min_dist = -1;
		void *buf_min = alloca(m_prmtv_user_data_len),
			 *buf_tmp = alloca(m_prmtv_user_data_len);

		Primitive *min_ptr = nullptr;

		for (auto &i: *(root->prmtv_list))
		{
			real_t d;
			if (i->get_intersection(ray, d, buf_tmp))
			{
				if ((!min_ptr || d < min_dist) &&
						root->box.test_inside_and_border(ray.get_point(d)))
				{
					if (min_ptr)
						min_ptr->free_user_data(buf_min);
					min_dist = d;
					min_ptr = i.get_ptr();
					memcpy(buf_min, buf_tmp, m_prmtv_user_data_len);
				} else
					i->free_user_data(buf_tmp);
			}
		}

		if (min_ptr)
		{
			auto ret = min_ptr->make_ray_task(ray, buf_min);
			return ret;
		}
		return nullptr;
	}

	Node *ch0 = root->chl,
		 *ch1 = root->chr;
	real_t d0, d1;
	bool is0, is1;
	if (!ch0->box.get_intersection(ray, d0, is0))
		ch0 = nullptr;
	if (!ch1->box.get_intersection(ray, d1, is1))
		ch1 = nullptr;

	if (fabs(d0 - d1) < EPS)
	{
		if (is1 && !is0)
			std::swap(ch0, ch1);
		else if (ch0 && ch1)
		{
			auto r0 = do_make_ray_task(ch0, ray),
				 r1 = do_make_ray_task(ch1, ray);
			if (!r0)
				return r1;
			if (!r1)
				return r0;
			if (r0->get_intersection() > r1->get_intersection())
				std::swap(r0, r1);
			delete r1;
			return r0;
		}
	} else if (d1 < d0)
		std::swap(ch0, ch1);

	if (ch0)
	{
		auto ret = do_make_ray_task(ch0, ray);
		if (ret)
			return ret;
	}

	if (ch1)
		return do_make_ray_task(ch1, ray);
	return nullptr;
}

const AABox& BSPTree::get_box() const
{
	return m_root->box;
}

BSPTree::Config BSPTree::Config::make_optimal(int npr, real_t min_box_size)
{
	return Config(
			(int)ceil(log(log(2)*npr)/log(2)),
			8, // (int)ceil(1 / log(2)),
			min_box_size);
}


// vim: syntax=cpp11.doxygen
