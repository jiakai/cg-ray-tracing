/*
 * $File: debug.cc
 * $Date: Sat May 05 14:32:27 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifdef CONFIG_DEBUG

#include "debug.hh"
#include "lib/objio.hh"

#include <libgen.h>

#include <cstdio>
#include <cstdarg>
#include <cstring>


MeshOutBase* new_debug_mesh_out(const char *name)
{
	static ObjOutput oo(fopen(CONFIG_DEBUG_OUTPUT_OBJ_FPATH, "w"));
	return oo.add_mesh(name);
}

void __print_debug__(const char *file, const char *func, int line, const char *fmt, ...)
{
	char *fbase = basename(strdupa(file));
	fprintf(stderr, "[debug %s@%s:%d] ", func, fbase, line);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fputc('\n', stderr);
}


#endif // CONFIG_DEBUG

// vim: syntax=cpp11.doxygen

