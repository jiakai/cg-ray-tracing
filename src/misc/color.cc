/*
 * $File: color.cc
 * $Date: Fri Apr 06 22:09:31 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "color.hh"

const Color
	Color::BLACK(0, 0, 0),
	Color::WHITE(1, 1, 1);

// vim: syntax=cpp11.doxygen

