/*
 * $File: camera.cc
 * $Date: Mon May 07 10:26:57 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "camera.hh"
#include "utils.hh"
#include "lib/thread.hh"

#include <cstdlib>

/*!
 * \brief random between [0, x)
 */
static inline real_t get_rand(real_t x)
{
	return rand() / (RAND_MAX + 1.0) * x;
}


struct Camera::StatData
{
	int call_cnt = 0, ndof_tot = 0, ndof_max = 0;
	thread::MutexLock lock;
};

Camera::Camera(const RefCntPtr<Scene> &scene, const Config &conf):
	m_scene(scene), m_conf(conf),
	m_sensor_normal(conf.sensor_du.cross(conf.sensor_dv).get_normalized()),
	m_du_normal(m_conf.sensor_du.get_normalized()),
	m_dv_normal(m_conf.sensor_dv.get_normalized()),
	m_stat(new StatData)
{
	sassert(!m_sensor_normal.is_zero());
	m_sensor_normal.normalize();
}

Camera::~Camera()
{
	delete m_stat;
}

Color Camera::get_pixel(real_t u, real_t v) const
{
	Vector snsrp = m_conf.sensor_p0 + m_conf.sensor_du * u + m_conf.sensor_dv * v,
		   dir(m_conf.view_point, snsrp);
	dir.normalize();
	if (m_conf.dof_nray_min < 2)
		return m_scene->ray_tracing(Ray(snsrp, dir));

	Vector fplane_p0(m_conf.view_point + dir * (m_conf.focal_dist / dir.dot(m_sensor_normal)));
	Color sum;
	int cnt;
	for (cnt = 1; cnt <= m_conf.dof_nray_max; cnt ++)
	{
		Vector vs(snsrp + get_rand_unit() * get_rand(m_conf.aperture));
		Color prev_sum(sum);
		sum += m_scene->ray_tracing(Ray(vs, (fplane_p0 - vs).get_normalized()));

		if (cnt >= m_conf.dof_nray_min && (sum / cnt -
					prev_sum / (cnt - 1)).mod() < m_conf.dof_error)
			break;
	}
	sum = sum / cnt;

	m_stat->lock.enter();
	m_stat->call_cnt ++;
	m_stat->ndof_tot += cnt;
	update_max(m_stat->ndof_max, cnt);
	m_stat->lock.exit();

	return sum;
}

Vector Camera::get_rand_unit() const
{
	real_t phy = get_rand(M_PI * 2);
	return m_du_normal * cos(phy) + m_dv_normal * sin(phy);
}

void Camera::Config::trace_point(const Vector &target, const Vector &du_plane_norm,
		real_t du_mod, real_t dist, real_t ratio)
{
	Vector dir(view_point, target);
	sensor_du = dir.cross(du_plane_norm);
	sassert(!sensor_du.is_zero());
	sensor_du = sensor_du.get_normalized() * du_mod;
	sensor_dv = dir.cross(sensor_du).get_normalized() * (du_mod / ratio);
	sensor_p0 = view_point + dir.get_normalized() * dist + sensor_du * (-0.5) + sensor_dv * (-0.5);
}

void Camera::Config::set_focus(const Vector &target)
{
	focal_dist = Vector(view_point, target).dot(sensor_du.cross(sensor_dv).get_normalized());
	sassert(focal_dist > EPS);
}

int Camera::get_max_dof_nray() const
{
	return m_stat->ndof_max;
}

double Camera::get_avg_dof_nray() const
{
	return double(m_stat->ndof_tot) / m_stat->call_cnt;
}

// vim: syntax=cpp11.doxygen

