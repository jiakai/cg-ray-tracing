/*
 * $File: utils.cc
 * $Date: Fri May 04 11:40:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"

#include <cstddef>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>

HWTimer::HWTimer()
{
	gettimeofday(&m_start, nullptr);
}

double HWTimer::get_sec() const
{
	timeval tv;
	gettimeofday(&tv, nullptr);
	double ret = tv.tv_sec - m_start.tv_sec +
		1e-6*(tv.tv_usec - m_start.tv_usec);
	if (ret < 0)
		ret += 24 * 3600;
	return ret;
}

void __sassert_check__(bool val, const char *expr, const char *file,
		const char *func, int line)
{
	if (val)
		return;
	fprintf(stderr, "assertion \"%s\" failed, in function %s, defined at %s:%d\n",
			expr, func, file, line);
	abort();
}

char* ssprintf(const char *fmt, ...)
{
	static char buf[Conf::SSPRINTF_BUF_LEN];

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, Conf::SSPRINTF_BUF_LEN, fmt, ap);
	va_end(ap);

	return buf;
}

void error_exit(const char *msg)
{
	fprintf(stderr, "error: %s\n", msg);
	abort();
}

// vim: syntax=cpp11.doxygen

