/*
 * $File: mkcamera.cc
 * $Date: Sun Jun 10 08:46:37 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */


#define MESH_MODEL		"data/model.obj"
#define ANIM_MODEL		"data/animscene.obj"
#define TEXTURE_PATH	"data/texture.jpg"


#include "mkcamera.hh"
#include "camera.hh"

#include "debug.hh"

#include "lib/objio.hh"
#include "lib/streamio.hh"

#include "primitives/plane.hh"
#include "primitives/sphere.hh"
#include "primitives/bspt_holder.hh"

#include "textures/uniform.hh"
#include "textures/grid.hh"
#include "textures/image.hh"

#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;

#define DEF_STATIC(v) decltype(v) v
#define DEF_INS(c) DEF_STATIC(c::_ins)

class SceneMakerBase
{
	protected:
		Scene *m_scene;

		virtual void on_init(Camera::Config &camconf)
		{
			camconf.view_point = Vector(0, 0, -4.5);
			camconf.sensor_p0 = Vector(-1.5, -1, -3);
			camconf.sensor_du = Vector(3, 0, 0);
			camconf.sensor_dv = Vector(0, 3 / Conf::IMAGE_RATIO, 0);

			camconf.dof_nray_min = 0;
		}

	public:

		static vector<SceneMakerBase*> instance;

		SceneMakerBase()
		{
			instance.push_back(this);
		}

		SceneMakerBase& set_arg(Camera::Config &camconf, Scene *scene)
		{
			m_scene = scene;
			on_init(camconf);
			return *this;
		}

		virtual void run() = 0;
		virtual const char* get_desc() = 0;

		virtual ~SceneMakerBase() {}
};
DEF_STATIC(SceneMakerBase::instance);


// f{{{ ObjScene
class ObjScene: public virtual SceneMakerBase
{
	static ObjScene _ins;
	protected:

		vector<RefCntPtr<PrimitiveBase>> m_prmptv;

		class ObjReader: public ObjInput
		{
			ObjScene &m_scene;

			protected:

				void on_new_primitive(const RefCntPtr<PrimitiveBase> &prmtv)
				{
					m_scene.m_prmptv.push_back(prmtv);
				}

			public:

				ObjReader(ObjScene &scene):
					m_scene(scene)
				{}
		};
		friend class ObjReader;

	public:
		void run()
		{
			m_scene->add_point_light_source(Scene::PointLightSource(
						Vector(-1e5, -1e5, -1e5), Color(1, 0, 0), 1));
			m_scene->add_point_light_source(Scene::PointLightSource(
						Vector(-1e5, 1e5, -1e5), Color(0, 1, 0), 1));
			m_scene->add_point_light_source(Scene::PointLightSource(
						Vector(1e5, -1e5, -1e5), Color(0, 0, 1), 1));
			ObjReader r(*this);
			r.work(MESH_MODEL);

			auto h = new BSPTHolderPrimitive(BSPTree::Config::make_optimal(m_prmptv.size()));
			for (auto &i: m_prmptv)
				h->add_primitive(i);

			h->finish_add();
			m_scene->add_primitive(h);
		}

		const char* get_desc()
		{ return "render " MESH_MODEL; }
};
DEF_INS(ObjScene);

// f}}}

// f{{{ SphereDemoScene
class SphereDemoScene: public virtual SceneMakerBase
{
	static SphereDemoScene _ins;

	protected:
		const int
			NSPHERE = 2012,
			HEIGHT = 10;
		const real_t
			DTHETA = 0.1, R = 1.1, R0 = 4,
			THETA_CLEAR0 = M_PI * 4 / 3,
			THETA_CLEAR1 = M_PI * 5 / 3,
			R_MAX = R0 + R * NSPHERE / HEIGHT * DTHETA * 9 / 8,
			CAMERA_VP_DIST = R_MAX * 1.5,	// view point dist from (0, 0, 0)
			CAMERA_DU_MOD = 1.8,
			CAMERA_SV_DIST = 2,	// dist between sensor and view point
			LENSE_SPHERE_THETA = 1.525 * M_PI,
			MIRROR_SPHERE_THETA = 1.54 * M_PI,
			LENSE_SPHERE_R = R_MAX + 4,
			MIRROR_SPHERE_R = R_MAX - 1;
		
		bool MAKE_MIRROR = true, MAKE_LENSE = true;

		Sphere m_lense_sphere, m_mirror_sphere;

		void on_init(Camera::Config &camconf)
		{
			camconf.view_point = Vector(0, -3, -CAMERA_VP_DIST);
			camconf.trace_point(Vector(0, -10, 0),
					Vector(0, -1, 0), CAMERA_DU_MOD, CAMERA_SV_DIST, Conf::IMAGE_RATIO);
		}

		virtual BSPTHolderPrimitive* make_bspt()
		{
			return new BSPTHolderPrimitive(
					BSPTree::Config::make_optimal(NSPHERE + 5));
		}

		vector<Sphere> outline_sphere;

		SphereDemoScene():
			m_lense_sphere(Vector(
						LENSE_SPHERE_R * cos(LENSE_SPHERE_THETA),
						-3.4,
						LENSE_SPHERE_R * sin(LENSE_SPHERE_THETA)), 1.6),
			m_mirror_sphere(Vector(
						MIRROR_SPHERE_R * cos(MIRROR_SPHERE_THETA),
						-7.2,
						MIRROR_SPHERE_R * sin(MIRROR_SPHERE_THETA)), 2.3)
		{
			real_t theta = 0;
			int dof_sphere_t0 = 0;
			for (int t = 0, tmax = NSPHERE / HEIGHT; t < tmax; t ++)
			{
				real_t thm = fmod(theta, M_PI * 2);
				if (thm >= THETA_CLEAR0 && thm <= THETA_CLEAR1)
					theta += THETA_CLEAR1 - THETA_CLEAR0;
				else if (thm < THETA_CLEAR0 && thm >= M_PI)
					dof_sphere_t0 = outline_sphere.size();
				Vector ct(R0 + R * theta * cos(theta), 0, R0 + R * theta * sin(theta));
				for (int h = 0; h < HEIGHT; h ++)
				{
					ct.y = -1.01 * h - 0.6;
					Sphere sp(ct, 0.5);
					outline_sphere.push_back(sp);
				}
				theta += DTHETA;
			}
			LENSE_SPHERE_PTR = &m_lense_sphere;

			dof_sphere.clear();
			for (int i = 0; i < HEIGHT; i ++)
			{
				auto &sp = outline_sphere[dof_sphere_t0 + i];
				sp.set_radius(0.25);
				dof_sphere.push_back(sp);
			}
		}
	public:
		static const Sphere CENTER_SPHERE;
		static const Sphere* LENSE_SPHERE_PTR;
		static vector<Sphere> dof_sphere;

		void run()
		{
			m_scene->add_point_light_source(Scene::PointLightSource(
						Vector(-1e5, -1e5, -1e5), Color(1, 1, 1), 1));

			using opr_t = OpticalProperty;
			using mtl_t = PrimitiveBase::Material;
			using mtl_ptr_t = RefCntPtr<mtl_t>;
			using IllumMode = PrimitiveBase::Material::IllumMode;

			opr_t opr, opr_line;
			opr.shininess = 30;
			opr.specular = 0.8;
			opr.ambient = 0.7;
			opr.diffuse = Color(0.61, 0.53, 0.23) * 1.2;
			opr_line.ambient = opr.ambient;

			mtl_ptr_t mtl_grid(new mtl_t(new GridTexture(opr, opr_line)));

			opr.diffuse = Color(0, 1, 1) * 0.8;
			opr_line.diffuse = Color(1, 1, 0) * 0.8;
			opr.diffuse = Color::WHITE * 0.8;
			mtl_ptr_t mtl_grid_dense(new mtl_t(new GridTexture(opr, opr_line, 0.3, 0.3, 0.05)));
			mtl_grid_dense->rm_illum_mode(IllumMode::REFLECTION);
			m_scene->add_primitive(new PlanePrimitive(Plane(Vector(0, 1, 0), 0), mtl_grid));

			opr.transparency = 0.9;
			opr.shininess = 200;
			opr.specular = 0.08;
			opr.diffuse = Color::WHITE * 0.05;
			opr.ambient = 0;
			mtl_ptr_t mtl_trans(new mtl_t(new UniformTexture(opr), 1.517));
			mtl_trans->rm_illum_mode(IllumMode::SHADOW);

			opr.transparency = 0;
			opr.diffuse = Color::WHITE * 0.15;
			opr.specular = 1;
			mtl_ptr_t mtl_mirror(new mtl_t(new UniformTexture(opr)));


			BSPTHolderPrimitive *bspt = make_bspt();
			opr.shininess = 30;
			opr.specular = 0.2;
			opr.ambient = 0.2;
			mtl_ptr_t mtl_image(new mtl_t(
						new ImageTexture(TEXTURE_PATH, opr, 1 / (M_PI * 2) * 3, 1 / M_PI * 3)));
			mtl_image->rm_illum_mode(IllumMode::REFLECTION);
			bspt->add_primitive(new SpherePrimitive(CENTER_SPHERE, mtl_image));

			if (MAKE_LENSE)
				bspt->add_primitive(new SpherePrimitive(m_lense_sphere, mtl_trans));
			if (MAKE_MIRROR)
				bspt->add_primitive(new SpherePrimitive(m_mirror_sphere, mtl_mirror));

			int osp_cnt = 0;
			for (auto &i: outline_sphere)
				bspt->add_primitive(new SpherePrimitive(i, (osp_cnt ++) & 1 ? mtl_trans : mtl_grid_dense));

			bspt->finish_add();
			m_scene->add_primitive(bspt);

			print_debug("%s: nsphere=%d", get_desc(), outline_sphere.size() + 3);
		}

		const char* get_desc()
		{ return "sphere demo"; }
};
DEF_INS(SphereDemoScene);
DEF_STATIC(SphereDemoScene::dof_sphere);
DEF_STATIC(SphereDemoScene::LENSE_SPHERE_PTR);
const Sphere SphereDemoScene::CENTER_SPHERE(Vector(0, -5.2, 0), 5);

// f}}}

// f{{{ DOFSphereDemoScene
class DOFSphereDemoScene: public virtual SphereDemoScene
{
	static DOFSphereDemoScene _ins;

	protected:
		void on_init(Camera::Config &camconf)
		{
			SphereDemoScene::on_init(camconf);

			camconf.dof_nray_min = 3;
			camconf.set_focus(m_lense_sphere.get_center() +
					Vector(m_lense_sphere.get_center(), camconf.view_point).get_normalized() *
					(m_lense_sphere.get_radius() * 0.8));
			camconf.aperture = 0.05;
			camconf.dof_error = 0.005;

			print_debug("DOF: focal_dist=%.3lf aperture=%.3lf",
					camconf.focal_dist, camconf.aperture);
		}

	public:
		
		const char* get_desc()
		{ return "sphere demo with DOF"; }
};
DEF_INS(DOFSphereDemoScene);

// f}}}

// f{{{ AnimationScene
class AnimationScene: public ObjScene, public DOFSphereDemoScene
{
	static AnimationScene _ins;

	AnimationScene()
	{
		// MAKE_MIRROR = false;
		// MAKE_LENSE = false;
	}

	protected:
		virtual BSPTHolderPrimitive* make_bspt()
		{
			auto ret = new BSPTHolderPrimitive(
					BSPTree::Config::make_optimal(NSPHERE + m_prmptv.size() + 1));
			for (auto &i: m_prmptv)
				ret->add_primitive(i);
			return ret;
		}

		class PhaseBase
		{
			public:
				static vector<PhaseBase*> instance;
				PhaseBase() {instance.push_back(this);}
				virtual const char* get_desc() = 0;
				virtual void init(Camera::Config &camconf, real_t time) = 0;
				virtual ~PhaseBase() {}
		};

#define COMBINE(a, b) ((a) * (1 - time) + (b) * time)
		class FarToTeapotPhase: public PhaseBase
		{
			public:

				static FarToTeapotPhase _ins;
				const Vector P0{-80, -80, -80},
					  P1{0, -2, -40},
					  TARGET{-0.1, -1.2, -16};
				const real_t CAMERA_SV_DIST = 4,
					  CAMERA_DU_MOD = 2.2;
				FarToTeapotPhase() {}
				const char* get_desc() { return "from far to teapot"; }
				void init(Camera::Config &camconf, real_t time)
				{
					camconf.view_point = COMBINE(P0, P1);
					camconf.trace_point(TARGET, Vector(0, -1, 0),
							CAMERA_DU_MOD, CAMERA_SV_DIST, Conf::IMAGE_RATIO);
				}
		};
		class TransferTargetPhase: public PhaseBase
		{
			TransferTargetPhase():
				TARGET(SphereDemoScene::CENTER_SPHERE.get_center())
			{
				PVIEW = FarToTeapotPhase::_ins.P1;
				PVIEW.y = TARGET.y;
				TARGET.y -= SphereDemoScene::CENTER_SPHERE.get_radius() * 0.2;
			}
			public:
				static TransferTargetPhase _ins;
				Vector TARGET;
				const real_t CAMERA_SV_DIST = 2,
					  CAMERA_DU_MOD = FarToTeapotPhase::_ins.CAMERA_DU_MOD;
				Vector PVIEW;

				const char* get_desc() { return "transfer track target from teapot to center shphere"; }
				void init(Camera::Config &camconf, real_t time)
				{
					auto &ins = FarToTeapotPhase::_ins;
					camconf.view_point = COMBINE(ins.P1, PVIEW * time);
					camconf.trace_point(COMBINE(ins.TARGET, TARGET),
							Vector(0, -1, 0), 
							ins.CAMERA_DU_MOD,
							ins.CAMERA_SV_DIST * (1 - time) +
							CAMERA_SV_DIST * time,
							Conf::IMAGE_RATIO);
				}
		};
		class MoveAroundPhase: public PhaseBase
		{
			MoveAroundPhase():
				TARGET(SphereDemoScene::LENSE_SPHERE_PTR->get_center())
			{
				auto &sp = SphereDemoScene::dof_sphere.back();
				PVIEW = sp.get_center();
				PVIEW.y -= sp.get_radius() + 2;
				PVIEW.x -= sp.get_radius() + 0.2;
				PVIEW.z -= sp.get_radius() + 1.1;
			}
			public:
				static MoveAroundPhase _ins;
				Vector PVIEW, TARGET;

				const char *get_desc() { return "move around the scene"; }
				void init(Camera::Config &camconf, real_t time)
				{
					auto &ins = TransferTargetPhase::_ins;
					camconf.view_point = COMBINE(ins.PVIEW, PVIEW);
					camconf.trace_point(COMBINE(ins.TARGET, TARGET),
							Vector(0, -1, 0), 
							ins.CAMERA_DU_MOD,
							ins.CAMERA_SV_DIST,
							Conf::IMAGE_RATIO);
				}
		};
		class PrepareDofPhase: public PhaseBase
		{
			PrepareDofPhase():
				TARGET(SphereDemoScene::dof_sphere.front().get_center())
			{
			}

			public:
				static PrepareDofPhase _ins;

				const Vector TARGET;

				const char *get_desc() { return "change trace target to prepare for DOF demo"; }
				void init(Camera::Config &camconf, real_t time)
				{
					auto &ins = MoveAroundPhase::_ins;
					auto &ins_prev = TransferTargetPhase::_ins;
					camconf.view_point = ins.PVIEW;
					camconf.trace_point(COMBINE(ins.TARGET, TARGET),
							Vector(-1 * time, -1 * (1 - time), 0), 
							ins_prev.CAMERA_DU_MOD,
							ins_prev.CAMERA_SV_DIST,
							Conf::IMAGE_RATIO);
				}
		};
		class DOFOpenAperturePhase: public PhaseBase
		{
			DOFOpenAperturePhase():
				DOF_TARGET(SphereDemoScene::dof_sphere.front().get_center())
			{
			}

			public:
				static DOFOpenAperturePhase _ins;
				const real_t APERTURE = 0.12;
				const Vector DOF_TARGET;

				const char *get_desc() { return "open aperture!"; }
				void init(Camera::Config &camconf, real_t time)
				{
					PrepareDofPhase::_ins.init(camconf, 1);

					camconf.dof_nray_min = 3;
					camconf.dof_nray_max = 15;
					camconf.set_focus(DOF_TARGET);
					camconf.aperture = APERTURE * time;
					camconf.dof_error = 0.005;
				}
		};
		class DOFPhase: public PhaseBase
		{
			DOFPhase()
			{
				auto &sp = SphereDemoScene::dof_sphere.back();
				DOF_TARGET = sp.get_center();
				DOF_TARGET.y -= sp.get_radius();
			}

			public:
				static DOFPhase _ins;
				Vector DOF_TARGET;

				const char *get_desc() { return "do dof"; }
				void init(Camera::Config &camconf, real_t time)
				{
					auto &ins = DOFOpenAperturePhase::_ins;
					ins.init(camconf, 1);
					camconf.set_focus(COMBINE(ins.DOF_TARGET, DOF_TARGET));
				}
		};

#undef COMBINE

		void on_init(Camera::Config &camconf)
		{
			SphereDemoScene::on_init(camconf);
			printf("please select a phase:\n");
			auto &ins = PhaseBase::instance;
			for (size_t i = 0; i < ins.size(); i ++)
				printf("  %d - %s\n", i, ins[i]->get_desc());
			int choice;
			sassert(scanf("%d", &choice) && choice >= 0 && choice < (int)ins.size());
			double time;
			printf("please enter time [0,1]: ");
			sassert(scanf("%lf", &time));
			ins[choice]->init(camconf, time);
		}

	public:
		void run()
		{
			ObjScene::ObjReader reader(*this);
			reader.work(ANIM_MODEL);
			DOFSphereDemoScene::run();
		}

		const char* get_desc()
		{ return "animation scene"; }

};
DEF_INS(AnimationScene);
DEF_STATIC(AnimationScene::PhaseBase::instance);
DEF_INS(AnimationScene::FarToTeapotPhase);
DEF_INS(AnimationScene::TransferTargetPhase);
DEF_INS(AnimationScene::MoveAroundPhase);
DEF_INS(AnimationScene::PrepareDofPhase);
DEF_INS(AnimationScene::DOFOpenAperturePhase);
DEF_INS(AnimationScene::DOFPhase);
// f}}}

// f{{{ MRRenderScene
class MRRenderScene: public ObjScene
{
	static MRRenderScene _ins;
	Vector m_camview, m_camtarget;

	protected:

		void on_init(Camera::Config &camconf)
		{
			cin >> m_camview;
			camconf.view_point = m_camview;
			Vector norm;
			real_t mod, dist;
			cin >> m_camtarget >> norm >> mod >> dist;
			camconf.trace_point(m_camtarget, norm, mod, dist, Conf::IMAGE_RATIO);

			m_scene->set_max_depth(1);
		}

	public:
		void run()
		{
			m_scene->add_point_light_source(Scene::PointLightSource(
						m_camtarget + (m_camview - m_camtarget) * 2, Color(1, 1, 1), 1));
			ObjScene::ObjReader r(*this);
			string fpath;
			cin >> fpath;
			r.work(fpath.c_str());

			for (auto &i: m_prmptv)
				m_scene->add_primitive(i);
		}

		const char* get_desc()
		{
			return "renderer for mesh-reduction demo (read camera config and fpath from stdin)";
		}
};
DEF_INS(MRRenderScene);
// f}}}

Camera* mkcamera()
{
	Camera::Config camconf;

	int choice;
	auto &ins = SceneMakerBase::instance;
	if (!Conf::QUIET)
	{
		printf("Please choose a demo:\n");
		for (size_t i = 0; i < ins.size(); i ++)
			printf("  %d - %s\n", i, ins[i]->get_desc());
	}
	sassert(scanf("%d", &choice) && choice >= 0 && choice < (int)ins.size());

	Scene *scene = new Scene(
			0.1,	// min_weigh
			0.2,	// ambient_factor
			10);	// max_depth

	ins[choice]->set_arg(camconf, scene).run();

	scene->ray_tracing_init();

	return new Camera(scene, camconf);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

