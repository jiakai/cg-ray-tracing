/*
 * $File: file.cc
 * $Date: Mon Apr 23 13:55:14 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "lib/imgwriters/file.hh"

#include <string>

#include <Magick++.h>

using namespace std;
using namespace Magick;

class _FileImgWriterImpl
{
	Image m_image;
	Pixels *m_view;
	PixelPacket *m_pixels_ptr;

	int m_width;

	string m_fpath;

	public:
		
		_FileImgWriterImpl(const ImgWriter::Geometry &g, const char *fpath) :
			m_image(Magick::Geometry(g.width, g.height), "black"),
			m_width(g.width),
			m_fpath(fpath)
		{
			m_image.type(TrueColorType);
			m_image.modifyImage();
			m_view = new Pixels(m_image);
			m_pixels_ptr = m_view->get(0, 0, g.width, g.height);
		}

		~_FileImgWriterImpl()
		{
			delete m_view;
		}

		void write(int x, int y, const ::Color &pixel)
		{
			m_pixels_ptr[y * m_width + x] = ColorRGB(pixel.r, pixel.g, pixel.b);
		}

		void save()
		{
			m_view->sync();
			m_image.write(m_fpath.c_str());
		}
};

FileImgWriter::FileImgWriter(const ImgWriter::Geometry &g, const char *fpath) :
	ImgWriter(g), impl(new _FileImgWriterImpl(g, fpath))
{
}

FileImgWriter::~FileImgWriter()
{
	delete impl;
}

void FileImgWriter::write_impl(int x, int y, const ::Color &pixel)
{
	impl->write(x, y, pixel);
}

void FileImgWriter::finish()
{
	impl->save();
}

// vim: syntax=cpp11.doxygen

