/*
 * $File: imgwriter.cc
 * $Date: Mon Apr 23 13:55:06 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "lib/imgwriter.hh"
#include "utils.hh"

#define LOCK \
	thread::ScopedMutexLock __lock__(m_lock)

ImgWriter::ImgWriter(const Geometry &g) :
	m_pxl_cnt(0), m_geometry(g)
{
}

void ImgWriter::write(int x, int y, const Color &pixel)
{
	sassert(x >= 0 && x < m_geometry.width && y >= 0 && y < m_geometry.height);
	pixel.check_range();
	LOCK;
	write_impl(x, y, pixel);
	m_pxl_cnt ++;
}

int ImgWriter::get_pixel_cnt() const
{
	LOCK;
	return m_pxl_cnt;
}

// vim: syntax=cpp11.doxygen
