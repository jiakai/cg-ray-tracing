/*
 * $File: objio.cc
 * $Date: Thu Jun 07 20:05:49 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "debug.hh"
#include "lib/objio.hh"
#include "lib/streamio.hh"
#include "geometry/base.hh"
#include "primitives/mesh.hh"

#include "textures/uniform.hh"

#include <libgen.h>
#include <alloca.h>

#include <cstring>
#include <cstdio>
#include <cctype>

#include <map>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
using namespace std;

static void strip(string &str)
{
	if (str.empty())
		return;
	size_t p = 0;
	while (p < str.size() && isspace(str[p]))
		p ++;
	str.erase(0, p);
	while (!str.empty() && isspace(str.back()))
		str.pop_back();
}

class ObjOutput::MeshOut: public MeshOutBase
{
	ObjOutput &m_oo;
	int m_v0;

	public:
		MeshOut(ObjOutput &oo, const char *name):
			m_oo(oo), m_v0(oo.m_vtx_cnt + 1)
		{
			fprintf(m_oo.m_fout, "o %s\n", name);
		}

		void add_vtx(const Vector &p)
		{
			m_oo.m_vtx_cnt ++;
			fprintf(m_oo.m_fout, "v %.8lf %.8lf %.8lf\n",
					p.x, p.y, p.z);
		}

		void begin_face()
		{
			fprintf(m_oo.m_fout, "f ");
		}

		void add_face_vtx(int v)
		{
			fprintf(m_oo.m_fout, "%d ", v + m_v0);
		}

		void end_face()
		{
			fprintf(m_oo.m_fout, "\n");
		}
};

ObjOutput::ObjOutput(FILE *fout, bool close_on_dtor):
	m_fout(fout), m_close_on_dtor(close_on_dtor)
{
	sassert(fout);
}

ObjOutput::~ObjOutput()
{
	if (m_close_on_dtor)
		fclose(m_fout);
}

MeshOutBase* ObjOutput::add_mesh(const char *name)
{
	return new MeshOut(*this, name);
}



class ObjInput::FileReader
{
	ifstream m_fin;

	public:
		FileReader(const char *fpath):
			m_fin(fpath)
		{
			if (!m_fin)
			{
				fprintf(stderr, "failed to open obj file %s: %m", fpath);
				sassert(0);
			}
		}

		struct Line
		{
			string name;
			istringstream arg;
		};
		bool read_line(Line &line);
};

struct ObjInput::DataList
{
	struct Material
	{
		RefCntPtr<PrimitiveBase::Material> mtl;
		bool transparent = false;

		Material(const RefCntPtr<PrimitiveBase::Material> &m):
			mtl(m)
		{}

		Material()
		{}
	};
	map<string, Material> mtllib;
	vector<Vector> vtx;
};

class ObjInput::MeshMaker
{
	ObjInput::DataList *m_data_list;

	struct Face
	{
		int v[3];
	};

	map<int, int> m_vtx_num;
	vector<Face> m_face;
	vector<Vector> m_vtx;

	MeshPrimitive::Config m_conf;
	RefCntPtr<PrimitiveBase::Material> m_material;

	int get_vtx_num(int v)
	{
		auto p = m_vtx_num.find(v);
		if (p == m_vtx_num.end())
		{
			sassert(v >= 0 && v < (int)m_data_list->vtx.size());
			m_vtx.push_back(m_data_list->vtx[v]);
			int n = m_vtx_num.size();
			return m_vtx_num[v] = n;
		}
		return p->second;
	}

	string m_name;

	public:
		
		MeshMaker(ObjInput::DataList *data_list):
			m_data_list(data_list)
		{}

		string& name()
		{
			return m_name;
		}

		void add_face(int v0, int v1, int v2)
		{
			int v[3] = {v0, v1, v2};
			Face f;
			for (int i = 0; i < 3; i ++)
				f.v[i] = get_vtx_num(v[i]);

			m_face.push_back(f);
		}

		MeshPrimitive* make_mesh()
		{
			if (m_face.empty())
				return nullptr;
			sassert(m_material);
			auto ret = new MeshPrimitive(m_material, m_conf);
			for (auto &i: m_vtx)
				ret->add_vtx(i);
			for (auto &i: m_face)
				ret->add_face(i.v[0], i.v[1], i.v[2]);
			print_debug("new mesh: %s: nvtx=%d nface=%d", m_name.c_str(), m_vtx.size(), m_face.size());
			ret->finish_init();
			clear();
			return ret;
		}

		void set_material(const RefCntPtr<PrimitiveBase::Material> &m)
		{
			m_material = m;
		}

		void clear()
		{
			m_vtx_num.clear();
			m_face.clear();
			m_vtx.clear();
			m_conf = MeshPrimitive::Config();
			m_material = nullptr;
		}

		MeshPrimitive::Config& get_conf()
		{ return m_conf; }

};

void ObjInput::work(const char *fpath)
{
	int cnt = 0;

	m_data_list = new DataList;
	FileReader fin(fpath);

	MeshMaker mmaker(m_data_list);

	for (FileReader::Line line; fin.read_line(line); )
	{
		if (line.name == "mtllib")
		{
			string fname;
			line.arg >> fname;
			if (fname[0] == '/')
				parse_mtllib(fname.c_str());
			else
			{
				char *fp0 = strdupa(fpath);
				char *fp = (char*)alloca(strlen(fpath) + fname.length() + 5);
				sprintf(fp, "%s/%s", dirname(fp0), fname.c_str());
				parse_mtllib(fp);
			}
			continue;
		}

		if (line.name == "o")
		{
			auto ptr = mmaker.make_mesh();
			if (ptr)
			{
				cnt ++;
				on_new_primitive(ptr);
			}
			line.arg >> mmaker.name();
			continue;
		}

		if (line.name == "v")
		{
			Vector v;
			line.arg >> v;
			m_data_list->vtx.push_back(v);
			continue;
		}

		if (line.name == "usemtl")
		{
			string name;
			line.arg >> name;
			auto &lib = m_data_list->mtllib;
			auto p = lib.find(name);
			if (p == lib.end())
				error_exit(ssprintf("unknown mtl: %s", name.c_str()));
			if (p->second.transparent)
				mmaker.get_conf().is_volume = true;
			mmaker.set_material(p->second.mtl);
			continue;
		}

		if (line.name == "s")
		{
			string type;
			line.arg >> type;
			if (type == "off")
				mmaker.get_conf().smooth = false;
			else
				mmaker.get_conf().smooth = true;
			continue;
		}

		if (line.name == "f")
		{
			int v[3];
			for (int i = 0; i < 3; i ++)
			{
				line.arg >> v[i];
				v[i] --;
			}
			mmaker.add_face(v[0], v[1], v[2]);
			continue;
		}

		error_exit(ssprintf("unknown section in obj: %s", line.name.c_str()));
	}

	auto ptr = mmaker.make_mesh();
	if (ptr)
	{
		cnt ++;
		on_new_primitive(ptr);
	}

	delete m_data_list;

	print_debug("nmesh=%d", cnt);
}

void ObjInput::parse_mtllib(const char *fpath)
{
	FileReader fin(fpath);
	string cur_mat_name;

	OpticalProperty color;
	real_t ref_idx = 1.5;
	int illum_mode = 2;

	auto make = [&]()
	{
		if (!cur_mat_name.empty())
		{
			auto mat = new PrimitiveBase::Material(new UniformTexture(color), ref_idx);
			DataList::Material m(mat);
			using IllumMode = PrimitiveBase::Material::IllumMode;
			if (color.transparency > EPS)
			{
				mat->rm_illum_mode(IllumMode::SHADOW);
				m.transparent = true;
			}

			// XXX: my own illum specification
			if (illum_mode & 4)
				mat->rm_illum_mode(IllumMode::REFLECTION);
			if (illum_mode & 8)
				mat->rm_illum_mode(IllumMode::SHADOW);
			m_data_list->mtllib[cur_mat_name] = m;
		}
	};

	for (FileReader::Line line; fin.read_line(line); )
	{
		if (line.name == "newmtl")
		{
			make();
			line.arg >> cur_mat_name;
			continue;
		}

#define READ(n, val) \
		if (line.name == n) \
		{ \
			line.arg >> val; \
			continue; \
		} \

		READ("Ns", color.shininess);
		READ("Ka", color.ambient);
		READ("Kd", color.diffuse);
		READ("Ks", color.specular);
		READ("Ni", ref_idx);

#undef READ

		if (line.name == "d")
		{
			real_t d;
			line.arg >> d;
			color.transparency = 1 - d;
			continue;
		}

		if (line.name == "illum")
		{
			line.arg >> illum_mode;
			continue;
		}

		error_exit(ssprintf("unknown section in mtl: %s", line.name.c_str()));
	}
	make();
}

bool ObjInput::FileReader::read_line(Line &line)
{
	for (string l; getline(m_fin, l); )
	{
		strip(l);
		if (l.empty() || l[0] == '#')
			continue;

		line.arg.clear();
		line.arg.str(l);
		line.arg >> line.name;
		return true;
	}
	return false;
}
// vim: syntax=cpp11.doxygen

