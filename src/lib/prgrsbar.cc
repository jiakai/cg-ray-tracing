/*
 * $File: prgrsbar.cc
 * $Date: Fri Jun 08 00:08:47 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "lib/prgrsbar.hh"
#include "lib/thread.hh"
#include "utils.hh"

#include <cstdio>
#include <ctime>

#include <unistd.h>

static const int REFRESH_TIME = 100000; ///<! 进度条刷新间隔时间，微秒

static void worker_thread(const ImgWriter *writer);

void show_console_progressbar(const ImgWriter &writer)
{
	thread::Thread t(std::bind(worker_thread, &writer));
}

void worker_thread(const ImgWriter *writer)
{
	int area = writer->get_geometry().get_area();
	HWTimer timer;
	clock_t start_cpu = clock();
	printf(" --- ");
	for(; ;)
	{
		int cur = writer->get_pixel_cnt();
		double prog = cur / double(area);
		printf("\r%.2lf %% [elapsed %.3lf sec; ETA ", prog * 100, timer.get_sec());
		if (!cur)
			printf("N/A");
		else
			printf("%.2lf sec", timer.get_sec() * (1 / prog - 1));
		printf("]       ");
		fflush(stdout);
		if (cur == area)
		{
			printf("\rTotal CPU time: %.2lf sec  Real time: %.2lf sec\n", 
					(clock() - start_cpu) / double(CLOCKS_PER_SEC),
					timer.get_sec());
			return;
		}
		usleep(REFRESH_TIME);
	}
}

// vim: syntax=cpp11.doxygen

