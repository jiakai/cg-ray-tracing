/*
 * $File: thread.cc
 * $Date: Mon Apr 23 13:55:20 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "lib/thread.hh"
#include "utils.hh"

#include <set>
#include <cstring>
#include <cstdio>


using namespace std;
using namespace thread;

static set<pthread_t> active_threads;
static MutexLock active_threads_lock;


#define check_ret(expr) \
do \
{ \
	int ret = (expr); \
	if (ret) \
	{ \
		fprintf(stderr, "\nval: %d  error: %s\n", ret, strerror(ret)); \
		sassert(!ret); \
	} \
} while(0)

struct CallFuncArg
{
	Thread::func_t func;

	CallFuncArg(const Thread::func_t &f):
		func(f)
	{}
};
static void* call_func(void *arg);

MutexLock::MutexLock()
{
	check_ret(pthread_mutex_init(&m_lock, nullptr));
}

MutexLock::~MutexLock()
{
	check_ret(pthread_mutex_destroy(&m_lock));
}

void MutexLock::enter()
{
	check_ret(pthread_mutex_lock(&m_lock));
}

void MutexLock::exit()
{
	check_ret(pthread_mutex_unlock(&m_lock));
}

ScopedMutexLock::ScopedMutexLock(MutexLock &lock):
	m_lock(lock)
{
	m_lock.enter();
}

ScopedMutexLock::~ScopedMutexLock()
{
	m_lock.exit();
}

Thread::Thread(const Thread::func_t &func)
{
	CallFuncArg *arg = new CallFuncArg(func);
	check_ret(pthread_create(&m_id, nullptr, call_func, arg));

	active_threads_lock.enter();
	active_threads.insert(m_id);
	active_threads_lock.exit();
}

Thread::Thread(const Thread &t):
	m_id(t.m_id)
{ }

Thread& Thread::operator = (const Thread &t)
{
	m_id = t.m_id;
	return *this;
}

void Thread::join()
{
	check_ret(pthread_join(m_id, nullptr));
}

void Thread::wait_all()
{
	for (; ;)
	{
		pthread_t p0;

		active_threads_lock.enter();
		if (active_threads.empty())
		{
			active_threads_lock.exit();
			return;
		}
		p0 = *active_threads.begin();
		active_threads_lock.exit();

		check_ret(pthread_join(p0, nullptr));
	}
}

void* call_func(void *arg_)
{
	CallFuncArg *arg = static_cast<CallFuncArg*>(arg_);

	arg->func();

	delete arg;

	pthread_t self = pthread_self();

	pthread_detach(self);

	active_threads_lock.enter();
	active_threads.erase(self);
	active_threads_lock.exit();
	return nullptr;
}

// vim: syntax=cpp11.doxygen
