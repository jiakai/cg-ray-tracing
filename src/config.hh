/*
 * $File: config.hh
 * $Date: Fri Jun 08 00:03:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 编译时的静态配置选项
 *
 * 注：该文件在已通过编译选项包含，无需再在源代码中include之
 */

#define CONFIG_DEBUG					///<! 是否打开调试模式

#define CONFIG_DEBUG_OUTPUT_OBJ_FPATH "output/debug.obj"

//#define CONFIG_DEBUG_BSPT_TRACEROUTE	///<! 在 BSPTree 中是否输出光线路径 
//#define CONFIG_DEBUG_MESH_LOG_INTERSECT	///<! 在 MeshPrimitive 中输出最终与光线相交的三角面片
//#define CONFIG_MESH_LOG_NORM			///<! 在MeshPrimitive 中输出计算出的各面和各顶点的法向量
//#define CONFIG_DEBUG_LOG_RAY	///<! RayTracing 中是否记录光线路径
//#define CONFIG_DEBUG_NO_SHADOW			///<! 不进行阴影测试，以减少调试时干扰


namespace Conf
{
	static constexpr int
		SSPRINTF_BUF_LEN = 2048;
	extern int IMAGE_RATIO_W, IMAGE_RATIO_H;
	extern double IMAGE_RATIO;
	extern bool QUIET;
}

// vim: syntax=cpp11.doxygen

