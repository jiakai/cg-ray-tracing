/*
 * $File: sphere.cc
 * $Date: Mon May 07 15:55:48 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "primitives/sphere.hh"

#include <cassert>

class SpherePrimitive::RayTask: public PrimitiveBase::RayTask
{
	const SpherePrimitive &m_sphere;
	Sphere::RayTask *m_if;

	public:

		RayTask(const SpherePrimitive &sphere, Sphere::RayTask *rt_if):
			PrimitiveBase::RayTask(&sphere), m_sphere(sphere), m_if(rt_if)
		{}

		~RayTask()
		{ delete m_if; }

		real_t get_intersection()
		{ return m_if->get_intersection(); }

		Vector get_normal()
		{ return m_if->get_normal(); }

		void get_transmission(Ray &ret);
		void get_opt_prpt(AutoPtr<const OpticalProperty> &ptr);
};

void SpherePrimitive::RayTask::get_transmission(Ray &ret)
{
	if (m_if->is_tangent())
	{
		ret = m_if->get_ray();
		return;
	}
	real_t n = m_sphere.m_material->get_refractive_idx();
	if (m_if->is_inside() || m_if->is_on_border())
		n = 1 / n;
	ret.src = m_if->get_ray().get_point(m_if->get_intersection());
	if (!get_tr_by_refractive_idx(ret.dir, n, m_if->get_ray().dir, m_if->get_normal()))
		ret.dir = -m_if->get_normal().get_reflection(m_if->get_ray().dir);
}

void SpherePrimitive::RayTask::get_opt_prpt(AutoPtr<const OpticalProperty> &ptr)
{
	Vector v(m_if->get_ray().get_point(m_if->get_intersection()) - m_sphere.m_sphere.get_center());
	return m_sphere.m_material->get_texture().get_opt_prpt(
			atan2(v.z, v.x) + M_PI,
			acos(-v.y / v.mod()),
			ptr);
}

SpherePrimitive::SpherePrimitive(const Sphere &sphere, const RefCntPtr<Material> &material):
	PrimitiveBase(material), m_sphere(sphere)
{
}


PrimitiveBase::RayTask* SpherePrimitive::make_ray_task(const Ray &ray) const
{
	if (!ray.test_visible(ray, *this))
		return nullptr;
	Sphere::RayTask *rtif = m_sphere.make_ray_task(ray);
	if (rtif)
		return new SpherePrimitive::RayTask(*this, rtif);
	return nullptr;
}

bool SpherePrimitive::test_intersect(const AABox &box) const
{
	real_t sum = 0;
	Vector ct(m_sphere.get_center()),
		   v0(box.get_v0()), v1(box.get_v1());
	for (int i = 0; i < 3; i ++)
	{
		real_t c = ct.get_comp(i);
		if (c < v0.get_comp(i))
			sum += sqr(c - v0.get_comp(i));
		else if (c > v1.get_comp(i))
			sum += sqr(c - v1.get_comp(i));
	}
	return sum <= sqr(m_sphere.get_radius());
}

void SpherePrimitive::update_bounder(Vector &vmin, Vector &vmax) const
{
	const auto &b = m_sphere.get_bounding_box();
	vmin.update_min(b.get_v0());
	vmax.update_max(b.get_v1());
}

// vim: syntax=cpp11.doxygen

