/*
 * $File: mesh.cc
 * $Date: Thu Jun 07 19:43:11 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "primitives/mesh_impl.hh"
#include "debug.hh"
#include "utils.hh"

#include <limits>
#include <algorithm>

namespace MeshPrimitiveImpl
{
	struct RayTaskArg
	{
		bool inside;
		const Face *face;
		real_t dist, u, v;
	};
}
using namespace MeshPrimitiveImpl;

class MeshPrimitive::RayTask: public PrimitiveBase::RayTask
{
	bool m_normal_done = false;
	Vector m_normal;

	const MeshPrimitive &m_mesh;
	Ray m_ray;
	RayTaskArg m_arg;

	public:
		RayTask(const MeshPrimitive &mesh, const Ray &ray, const RayTaskArg &arg):
			PrimitiveBase::RayTask(&mesh), m_mesh(mesh), m_ray(ray), m_arg(arg)
		{}

		real_t get_intersection()
		{
#ifdef CONFIG_DEBUG_MESH_LOG_INTERSECT
			static int cnt = 0;
			auto out = new_debug_mesh_out(ssprintf("MeshPrimitive::its.tri.%d", cnt ++));
			m_arg.face->triangle.dump_mesh(*out);
			delete out;
			print_debug("d=%.6e u=%.6e v=%.6e f=0x%x", m_arg.dist, m_arg.u, m_arg.v,
					(int)m_arg.face);
#endif // CONFIG_DEBUG_MESH_LOG_INTERSECT
			return m_arg.dist;
		}

		Vector get_normal()
		{
			if (m_normal_done)
				return m_normal;
			m_normal_done = true;

			if (m_mesh.m_conf.smooth)
			{
				const Face &f = *m_arg.face;
				real_t u = m_arg.u, v = m_arg.v;
				m_normal = (f.vtx[0]->normal * (1 - u - v) +
						f.vtx[1]->normal * u +
						f.vtx[2]->normal * v);
				m_normal.normalize();

			} else
				m_normal = m_arg.face->normal;

			if (m_ray.dir.dot(m_normal) > 0)
				m_normal = -m_normal;

			return m_normal;
		}

		void get_transmission(Ray &ret)
		{
			sassert(m_mesh.m_conf.is_volume);
			real_t n = m_mesh.m_material->get_refractive_idx();
			if (m_arg.inside)
				n = 1 / n;

			ret.src = m_ray.get_point(m_arg.dist);
			if (!get_tr_by_refractive_idx(ret.dir, n, m_ray.dir, get_normal()))
				ret.dir = -m_normal.get_reflection(m_ray.dir);
		}

		void get_opt_prpt(AutoPtr<const OpticalProperty> &ptr)
		{
			// XXX: uniform color
			auto v = m_arg.face->vtx;
			Vector2D mapped(v[0]->map_coord * m_arg.u +
					v[1]->map_coord * m_arg.v +
					v[2]->map_coord * (1 - m_arg.u - m_arg.v));
			m_mesh.m_material->get_texture().get_opt_prpt(
					mapped.x, mapped.y, ptr);
		}
};


class MeshPrimitive::BSPTreePrimitive: public BSPTree::Primitive
{
	const MeshPrimitive &m_mesh;
	MeshPrimitiveImpl::Face *m_face;

	public:
		
		BSPTreePrimitive(const MeshPrimitive &mesh, MeshPrimitiveImpl::Face *face):
			m_mesh(mesh), m_face(face)
		{ }

		bool test_intersect(const AABox &box) const
		{
			return m_face->triangle.test_intersect(box);
		}

		int get_user_data_size() const
		{ return sizeof(RayTaskArg); }

		bool get_intersection(const Ray &ray, real_t &dist,
				void *user_data) const
		{
			RayTaskArg *rtarg = static_cast<RayTaskArg*>(user_data);
			if (m_face->triangle.test_intersect(ray, dist, rtarg->u, rtarg->v) && dist >= EPS)
			{
				rtarg->dist = dist;
				return true;
			}
			return false;
		}

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray, void *user_data) const
		{
			RayTaskArg *rtarg = static_cast<RayTaskArg*>(user_data);
			if (!m_mesh.m_conf.is_volume)
				rtarg->inside = false;
			else
				rtarg->inside = ray.dir.dot(m_face->normal) > 0;
			rtarg->face = m_face;
			return new MeshPrimitive::RayTask(m_mesh, ray, *rtarg);
		}

		void free_user_data(void *user_data) const
		{}
};

MeshPrimitive::MeshPrimitive(const RefCntPtr<Material> &material, const Config &conf):
	PrimitiveBase(material), m_data_list(new DataList), 
	m_conf(conf)
{
}

MeshPrimitive::~MeshPrimitive()
{
	delete m_data_list;
}

void MeshPrimitive::add_vtx(const Vector &p)
{
	auto &vtx = m_data_list->vtx;
	vtx.push_back(Vertex(p));
}

void MeshPrimitive::set_vtx_coord_map(int v, const Vector2D &p)
{
	assert(v >= 0 && v < (int)m_data_list->vtx.size());
	m_data_list->vtx[v].map_coord = p;
}

void MeshPrimitive::add_face(int v0, int v1, int v2)
{
	sassert(std::min(std::min(v0, v1), v2) >= 0 &&
			std::max(std::max(v0, v1), v2) < (int)m_data_list->vtx.size());
	m_data_list->face.push_back(Face(
			m_data_list->vtx[v0],
			m_data_list->vtx[v1],
			m_data_list->vtx[v2]));
}

void MeshPrimitive::finish_init()
{
	m_data_list->init(m_conf.is_volume);

	if (m_conf.auto_map_coord)
		m_data_list->calc_vtx_map_coord();

	if (m_conf.is_volume)
		m_data_list->regulate_face_norm(true);
	else if (m_conf.smooth)
		m_data_list->regulate_face_norm(false);

	if (m_conf.smooth)
	{
		for (auto &cur_vtx: m_data_list->vtx)
		{
			Vector vec_sum;
			real_t area_sum = 0;
			for (auto &i: cur_vtx.ext->adj_face)
			{
				real_t s = i->get_area();
				vec_sum += i->normal * s;
				area_sum += s;
			}
			(cur_vtx.normal = vec_sum * (1 / area_sum)).normalize();
		}
#ifdef CONFIG_MESH_LOG_NORM
		int cnt = 0;
		for (auto &v: m_data_list->vtx)
			debug_mesh_out_line_segment(ssprintf("mesh.vtx.%d.norm", cnt ++), v.coord, v.coord + v.normal);
#endif // CONFIG_MESH_LOG_NORM
	}

	Vector min, max;
	min.x = min.y = min.z = std::numeric_limits<real_t>::min();
	max.x = max.y = max.z = -min.x;

	for (auto &i: m_data_list->vtx)
		for (int c = 0; c < 3; c ++)
		{
			update_min(min.get_comp(c), i.coord.get_comp(c));
			update_max(max.get_comp(c), i.coord.get_comp(c));
		}

	for (int c = 0; c < 3; c ++)
		if (fabs(min.get_comp(c) - max.get_comp(c)) < EPS)
		{
			min.get_comp(c) -= 0.5;
			max.get_comp(c) += 0.5;
		}


	m_data_list->bsp_tree = new BSPTree(AABox(min, max),
			BSPTree::Config::make_optimal(m_data_list->face.size()));

	for (auto &i: m_data_list->face)
		m_data_list->bsp_tree->add_primitive(new BSPTreePrimitive(*this, &i));


	m_data_list->clear_extra_data();
}


PrimitiveBase::RayTask* MeshPrimitive::make_ray_task(const Ray &ray) const
{
#ifdef CONFIG_DEBUG_MESH_LOG_INTERSECT
	static int call_cnt = 0;
	real_t min_dist = std::numeric_limits<real_t>::max();
	Triangle *min_tri = nullptr;
	for (auto &face: m_data_list->face)
	{
		real_t d, u, v;
		if (face.triangle.test_intersect(ray, d, u, v))
		{
			print_debug("d=%.6e u=%.6e v=%.6e f=0x%x", d, u, v, (int)&face);
			if (d < min_dist)
			{
				min_dist = d;
				min_tri = &face.triangle;
			}
		}
	}
	call_cnt ++;
	if (min_tri)
	{
		auto mesh = new_debug_mesh_out(ssprintf("its.tri.correct.%d", call_cnt));
		min_tri->dump_mesh(*mesh);
		delete mesh;

		mesh = new_debug_mesh_out(ssprintf("its.tri.correct.%d.ray", call_cnt));
		mesh->add_line_segment(ray.src, ray.get_point(min_dist));
		delete mesh;
	}
#endif // CONFIG_DEBUG_MESH_LOG_INTERSECT

	if (!ray.test_visible(ray, *this))
		return nullptr;
	return m_data_list->bsp_tree->make_ray_task(ray);
}

bool MeshPrimitive::test_intersect(const AABox &box) const
{
	return m_data_list->bsp_tree->get_box().test_intersect(box);
}

void MeshPrimitive::update_bounder(Vector &vmin, Vector &vmax) const
{
	auto &b = m_data_list->bsp_tree->get_box();
	vmin.update_min(b.get_v0());
	vmax.update_max(b.get_v1());
}


// vim: syntax=cpp11.doxygen

