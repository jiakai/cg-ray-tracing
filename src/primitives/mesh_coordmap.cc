/*
 * $File: mesh_coordmap.cc
 * $Date: Thu May 03 16:34:01 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 实现 MeshPrimitive 中纹理映射的核心算法
 */

#include "primitives/mesh_impl.hh"
#include "utils.hh"
#include "debug.hh"

#include <cstring>
#include <list>

using namespace MeshPrimitiveImpl;

static const real_t
	TIME_SEGMENT = 1e-2,
	SPRING_CONSTANT = 1,
	MASS = 10,
	DAMPING_PARAMETER = 0.1;

static Vector2D simulate_move(const Vertex &vtx);

/*!
 * \brief compute the force acted on v0 by v1
 */
static inline Vector2D get_force(const Vertex &v0, const Vertex &v1);

template<typename Iterable>
real_t get_total_energy(const Iterable &vtx);



static Vector v2tov3(const Vector2D &v)
{
	return Vector(v.x, v.y, 0);
}

void MeshPrimitive::DataList::calc_vtx_map_coord()
{
	Vector center;
	for (auto &i: vtx)
		center += i.coord;
	center *= 1.0 / vtx.size();

	for (auto &i: vtx)
	{
		Vector dir(center, i.coord);
		i.map_coord.x = acos(dir.z / dir.mod());
		i.map_coord.y = atan2(dir.x, dir.y);
		i.ext->velocity.set_zero();
	}

	int niter = 0;
	std::vector<Vector2D> next_coord;
	next_coord.resize(vtx.size());
	real_t target_energy = get_total_energy(vtx) / M_E;
	do
	{
		niter ++;
		for (std::size_t i = 0; i < vtx.size(); i ++)
			next_coord[i] = simulate_move(vtx[i]);
		for (std::size_t i = 0; i < vtx.size(); i ++)
			vtx[i].map_coord = next_coord[i];
	} while (get_total_energy(vtx) > target_energy);


	print_debug("niter=%d", niter);

	auto mesh = new_debug_mesh_out("mapped_coord");
	for (auto &i: vtx)
		mesh->add_vtx(Vector(i.map_coord.x, i.map_coord.y, 0));
	for (auto &i: face)
	{
		mesh->begin_face();
		for (int  j = 0; j < 3; j ++)
			mesh->add_face_vtx(i.vtx[j]->ext->idx);
		mesh->end_face();
	}
	delete mesh;
}

Vector2D simulate_move(const Vertex &vtx)
{
	Vector2D f;
	for (auto &i: vtx.ext->adj_vtx)
		f += get_force(vtx, *i);
	f -= vtx.ext->velocity * DAMPING_PARAMETER;
	Vector2D a = f * (1 / MASS),
			 p1 = vtx.map_coord + vtx.ext->velocity * TIME_SEGMENT +
				 a * (0.5 * TIME_SEGMENT * TIME_SEGMENT);
	vtx.ext->velocity += a * TIME_SEGMENT;
	return p1;
}

Vector2D get_force(const Vertex &v0, const Vertex &v1)
{
	Vector2D dir(v0.map_coord, v1.map_coord);
	return dir.get_normalized() * ((dir.mod() - (v1.coord - v0.coord).mod()) * SPRING_CONSTANT);
}

template<typename Iterable>
real_t get_total_energy(const Iterable &vtx)
{
	real_t sum = 0;
	for (auto &i: vtx)
	{
		sum += MASS * i.ext->velocity.mod_sqr();
		for (auto &j: i.ext->adj_vtx)
			sum += get_force(i, *j).mod_sqr() / SPRING_CONSTANT;
	}
	return 0.5 * sum;
}

// vim: syntax=cpp11.doxygen

