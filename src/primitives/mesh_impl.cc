/*
 * $File: mesh_impl.cc
 * $Date: Fri Jun 08 00:22:04 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief MeshPrimitiveImpl 中一些主要函数的实现
 */

#include "primitives/mesh_impl.hh"

#include "debug.hh"
#include "utils.hh"
#include "lib/streamio.hh"

#include <cstdlib>

#include <map>
#include <list>
#include <algorithm>
using namespace std;

namespace MeshPrimitiveImpl
{
	struct Edge
	{
		Vertex *v0, *v1;

		Edge(Vertex *v0_, Vertex *v1_):
			v0(min(v0_, v1_)), v1(max(v0_, v1_))
		{ sassert(v0 != v1); }

		bool operator < (const Edge &e) const
		{ return v0 < e.v0 || (v0 == e.v0 && v1 < e.v1); }
	};
}

using namespace MeshPrimitiveImpl;

void MeshPrimitive::DataList::regulate_face_norm(bool test_dir)
{
	for (auto &i: face_ext)
		i.visited = false;

	for (auto &f0_test: face)
		if (!f0_test.ext->visited)
		{
			f0_test.ext->visited = true;
			if (test_dir)
				regulate_single_face_norm(f0_test);

			list<Face*> queue{&f0_test};

			while (queue.size())
			{
				Face &f0 = *queue.front();
				queue.pop_front();
				for (auto &f1: f0.ext->adj_face)
					if (!f1->ext->visited)
					{
						{
							f1->ext->visited = true;
							queue.push_back(f1);

							bool ok = false;
							for (int f0v = 0; f0v < 3 && !ok; f0v ++)
								for (int f1v = 0; f1v < 3; f1v ++)
									if (f0.vtx[f0v] == f1->vtx[f1v])
									{
										int f0comv, f1comv;
										for (f0comv = 0; f0comv < 3; f0comv ++)
											if (f0comv != f0v)
											{
												for (f1comv = 0; f1comv < 3; f1comv ++)
													if (f1comv != f1v && f0.vtx[f0comv] == f1->vtx[f1comv])
													{
														ok = true;
														break;
													}
												if (ok)
													break;
											}
										sassert(ok);

										int f0tv = 3 - f0v - f0comv, f1tv = 3 - f1v - f1comv;

										f1->normal = f1->get_norm_by_vtx_num(f1v, f1tv, f1comv).get_normalized();
										if (f0.normal.dot(f0.get_norm_by_vtx_num(f0v, f0comv, f0tv)) < 0)
											f1->normal = -f1->normal;

										break;
									}
							sassert(ok);
						}
					}
			}
		}

#ifdef CONFIG_MESH_LOG_NORM
	static int cnt = 0;
	for (auto &i: face)
	{
		auto mesh = new_debug_mesh_out(ssprintf("face.%d", cnt));
		i.triangle.dump_mesh(*mesh);
		delete mesh;
		mesh = new_debug_mesh_out(ssprintf("face.%d.norm", cnt ++));
		Vector vs(i.vtx[0]->coord * 0.3 + i.vtx[1]->coord * 0.3 + i.vtx[2]->coord * 0.4);
		mesh->add_line_segment(vs, vs + i.normal);
		delete mesh;
	}
#endif // CONFIG_MESH_LOG_NORM
}

void MeshPrimitive::DataList::regulate_single_face_norm(Face &f0)
{
	bool ok = false;
	int cnt = 0;
	while (!ok)
	{
		ok = true;
		real_t fac0 = rand(), fac1 = rand(), fac2 = rand(),
			   facsum = fac0 + fac1 + fac2;
		fac0 /= facsum; fac1 /= facsum; fac2 /= facsum;

		Vector norm_normal = f0.normal.get_normalized();
		Ray ray;
		ray.src = f0.vtx[0]->coord * fac0 + f0.vtx[1]->coord * fac1 +
			f0.vtx[2]->coord * fac2 + norm_normal * EPS;
		while(1)
		{
			ray.dir = Vector(rand() - RAND_MAX * 0.5, rand() - RAND_MAX * 0.5,
					rand() - RAND_MAX * 0.5).get_normalized();
			if (ray.dir.dot(norm_normal) > EPS)
				break;
		}

		cnt = 0;
		for (auto &i: face)
		{
			real_t dist, u, v;
			if (i.triangle.test_intersect(ray, dist, u, v) && dist > EPS)
			{
				cnt ++;
				if (fabs(u) < EPS || fabs(v) < EPS || fabs(1 - u - v) < EPS)
				{
					print_debug("warning: random ray touches border, retrying ...");
					ok = false;
				}
			}
		}
	}
	if (cnt & 1)
		f0.normal = -f0.normal;
}

void MeshPrimitive::DataList::clear_extra_data()
{
	vtx_ext.clear();
	face_ext.clear();
	for (auto &i: vtx)
		i.ext = nullptr;
	for (auto &i: face)
		i.ext = nullptr;
}

void MeshPrimitive::DataList::init(bool check_volume)
{
	vtx_ext.resize(vtx.size());
	face_ext.resize(face.size());

	for (size_t i = 0; i < vtx.size(); i ++)
		(vtx[i].ext = &vtx_ext[i])->idx = i;
	for (size_t i = 0; i < face.size(); i ++)
		face[i].ext = &face_ext[i];

	for (auto &f: face)
		for (int i = 0; i < 3; i ++)
			for (int j = 0; j < 3; j ++)
				if (i != j)
					f.vtx[i]->ext->adj_vtx.push_back(f.vtx[j]);


	map<Edge, list<Face*>> eaf; // edge adj face

	for (auto &cur_face: face)
		for (int i = 0; i < 3; i ++)
			for (int j = i + 1; j < 3; j ++)
			{
				Edge e(cur_face.vtx[i], cur_face.vtx[j]);
				auto edge_iter = eaf.find(e);
				if (edge_iter == eaf.end())
					eaf[e].push_back(&cur_face);
				else
				{
					for (auto &f: edge_iter->second)
					{
						f->add_adj_face(&cur_face);
						cur_face.add_adj_face(f);
					}
					edge_iter->second.push_back(&cur_face);
				}
			}

	for (auto &i: face)
		for (int j = 0; j < 3; j ++)
			i.vtx[j]->ext->adj_face.push_back(&i);


	if (check_volume)
	{
		int cnt = 0;
		for (auto &i: eaf)
			if (i.second.size() == 1)
				cnt ++;
		if (cnt)
			print_debug("%d faces have no adjacent face", cnt);
	}
}


// vim: syntax=cpp11.doxygen

