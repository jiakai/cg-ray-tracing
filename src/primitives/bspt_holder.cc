/*
 * $File: bspt_holder.cc
 * $Date: Thu May 03 20:47:05 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "primitives/bspt_holder.hh"
#include "utils.hh"

#include <vector>
#include <limits>

struct BSPTHolderPrimitive::DataList
{
	std::vector<RefCntPtr<PrimitiveBase>> prmtv;
};

class BSPTHolderPrimitive::BSPTreePrimitive: public BSPTree::Primitive
{
	RefCntPtr<PrimitiveBase> m_prmtv;

	struct UserData
	{
		PrimitiveBase::RayTask *rt;
	};

	public:
		
		BSPTreePrimitive(const RefCntPtr<PrimitiveBase> &prmtv):
			m_prmtv(prmtv)
		{}

		bool test_intersect(const AABox &box) const
		{
			return m_prmtv->test_intersect(box);
		}

		int get_user_data_size() const
		{ return sizeof(UserData); }


		bool get_intersection(const Ray &ray, real_t &dist,
				void *user_data) const
		{
			auto rt = (static_cast<UserData*>(user_data)->rt =
					m_prmtv->make_ray_task(ray));
			if (rt)
			{
				dist = rt->get_intersection();
				return true;
			}
			return false;
		}

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray, void *user_data) const
		{
			return static_cast<UserData*>(user_data)->rt;
		}

		void free_user_data(void *user_data) const
		{
			delete static_cast<UserData*>(user_data)->rt;
		}
};

BSPTHolderPrimitive::BSPTHolderPrimitive(const BSPTree::Config &conf):
	PrimitiveBase(nullptr), m_data_list(new DataList),
	m_bspt_conf(conf)
{
}

BSPTHolderPrimitive::~BSPTHolderPrimitive()
{
	delete m_data_list;
	delete m_tree;
}

void BSPTHolderPrimitive::add_primitive(const RefCntPtr<PrimitiveBase> &prmtv)
{
	m_data_list->prmtv.push_back(prmtv);
}

void BSPTHolderPrimitive::finish_add()
{
	Vector vmin, vmax;
	vmin.x = vmin.y = vmin.z = std::numeric_limits<real_t>::max();
	vmax.x = vmax.y = vmax.z = -vmin.x;

	for (auto &i: m_data_list->prmtv)
		i->update_bounder(vmin, vmax);

	m_tree = new BSPTree(AABox(vmin, vmax), m_bspt_conf);
	for (auto &i: m_data_list->prmtv)
		m_tree->add_primitive(new BSPTreePrimitive(i));

	delete m_data_list;
	m_data_list = nullptr;
}

PrimitiveBase::RayTask* BSPTHolderPrimitive::make_ray_task(const Ray &ray) const
{
	return m_tree->make_ray_task(ray);
}

bool BSPTHolderPrimitive::test_intersect(const AABox &box) const
{
	return m_tree->get_box().test_intersect(box);
}

void BSPTHolderPrimitive::update_bounder(Vector &vmin, Vector &vmax) const
{
	vmin.update_min(m_tree->get_box().get_v0());
	vmax.update_max(m_tree->get_box().get_v1());
}

// vim: syntax=cpp11.doxygen

