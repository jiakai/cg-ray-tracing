/*
 * $File: plane.cc
 * $Date: Mon May 07 15:49:18 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "primitives/plane.hh"

#include <cassert>

class PlanePrimitive::RayTask: public PrimitiveBase::RayTask
{
	const PlanePrimitive &m_plane;
	Ray m_ray;
	real_t m_dist; ///<! 光源到交点的位置

	public:

		RayTask(const PlanePrimitive &p, const Ray &ray, real_t dist):
			PrimitiveBase::RayTask(&p),
			m_plane(p), m_ray(ray), m_dist(dist)
		{
		}
		
		real_t get_intersection()
		{
			return m_dist;
		}

		Vector get_normal()
		{
			const Vector &n = m_plane.m_plane.get_normal();
			if (n.dot(m_ray.dir) > 0)
				return -n;
			return n;
		}

		void get_transmission(Ray &ret)
		{ ret = m_ray; }

		void get_opt_prpt(AutoPtr<const OpticalProperty> &ptr)
		{
			Vector it(m_ray.get_point(m_dist)),
				   dir(it - m_plane.m_p0);
			assert(m_plane.m_plane.test_on_plane(it));
			m_plane.m_material->get_texture().get_opt_prpt(
					dir.dot(m_plane.m_dir0), dir.dot(m_plane.m_dir1),
					ptr);
		}
};


PlanePrimitive::PlanePrimitive(const Plane &plane, const RefCntPtr<Material> &material,
		const Vector *txt_p0, const Vector *txt_dir0):
	PrimitiveBase(material), m_plane(plane)
{
	if (txt_p0)
		m_p0 = *txt_p0;
	else
		find_p0();

	if (txt_dir0)
		m_dir0 = txt_dir0->get_normalized();
	else
		find_dir0();

	sassert(m_plane.test_on_plane(m_p0));
	sassert(fabs(m_plane.get_normal().dot(m_dir0)) < EPS);
	sassert(fabs(m_dir0.mod_sqr() - 1) < EPS);

	m_dir1 = m_plane.get_normal().cross(m_dir0);
}

void PlanePrimitive::find_p0()
{
	const Vector &n = m_plane.get_normal();
	real_t c = m_plane.get_c();
	if (fabs(n.x) >= 0.5)
		m_p0.x = c / n.x;
	else if (fabs(n.y) >= 0.5)
		m_p0.y = c / n.y;
	else
		m_p0.z = c / n.z;
}

void PlanePrimitive::find_dir0()
{
	const Vector &n = m_plane.get_normal();
	real_t fy = fabs(n.y), fz = fabs(n.z);
	if (fy < EPS && fz < EPS)
		m_dir0.y = 1;
	else
	{
		m_dir0.x = 1;
		if (fy > fz)
			m_dir0.y = -n.x / n.y;
		else
			m_dir0.z = -n.x / n.z;
		m_dir0.normalize();
	}
}

PrimitiveBase::RayTask* PlanePrimitive::make_ray_task(const Ray &ray) const
{
	if (!ray.test_visible(ray, *this))
		return nullptr;
	real_t d;
	if (!m_plane.get_intersection(ray, d) || d < EPS)
		return nullptr;
	return new PlanePrimitive::RayTask(*this, ray, d);
}

// vim: syntax=cpp11.doxygen

