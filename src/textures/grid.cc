/*
 * $File: grid.cc
 * $Date: Sat Apr 14 21:26:23 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "textures/grid.hh"

#include <cmath>

void GridTexture::get_opt_prpt(real_t x, real_t y,
		AutoPtr<const OpticalProperty> &ptr) const
{
	x = fabs(fmod(x, m_grid_width));
	y = fabs(fmod(y, m_grid_height));

	if ((x <= m_border_width || x >= m_grid_width - m_border_width) ||
		(y <= m_border_width || y >= m_grid_height - m_border_width))
		ptr.set(&m_prpt_line, false);
	else
		ptr.set(&m_prpt_space, false);
}

// vim: syntax=cpp11.doxygen

