/*
 * $File: image.cc
 * $Date: Fri May 04 21:42:27 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "textures/image.hh"

#include <Magick++.h>
using namespace Magick;

ImageTexture::ImageTexture(const char *fpath, const OpticalProperty &base_opt_prpt,
		real_t x_scale, real_t y_scale):
	m_base_opt_prpt(base_opt_prpt), m_x_scale(x_scale), m_y_scale(y_scale)
{
	Image img(fpath);
	Geometry size = img.size();
	m_width = size.width();
	m_height = size.height();

	m_pixels = new ::Color[m_width * m_height];

	::Color *dest = m_pixels;

	const PixelPacket* src = img.getConstPixels(0, 0, m_width, m_height);
	for (int y = 0; y < m_height; y ++)
		for (int x = 0; x < m_width; x ++)
		{
			dest->r = double(src->red) / QuantumRange;
			dest->g = double(src->green) / QuantumRange;
			dest->b = double(src->blue) / QuantumRange;
			dest ++;
			src ++;
		}
}

ImageTexture::~ImageTexture()
{
	delete []m_pixels;
}

void ImageTexture::get_opt_prpt(real_t x, real_t y,
		AutoPtr<const OpticalProperty> &ptr) const
{
	x *= m_x_scale;
	y *= m_y_scale;
	if (m_transform_coordinate)
		m_transform_coordinate(x, y);
	x = fmod(x, 1);
	y = fmod(y, 1);
	sassert(x >= 0 && y >= 0);

	auto ret = new OpticalProperty(m_base_opt_prpt);
	ret->diffuse = m_pixels[(int)(y * m_height) * m_width + (int)(x * m_width)] * (1 - ret->transparency);

	ptr.set(ret, true);
}


// vim: syntax=cpp11.doxygen

