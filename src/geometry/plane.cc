/*
 * $File: plane.cc
 * $Date: Sun Apr 22 21:55:54 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geometry/plane.hh"

Vector Plane::find_point() const
{
	Vector ret;
	const Vector &n = m_normal;
	if (fabs(n.x) >= 0.5)
		ret.x = m_c / n.x;
	else if (fabs(n.y) >= 0.5)
		ret.y = m_c / n.y;
	else
		ret.z = m_c / n.z;
	return ret;
}

Vector Plane::find_vector() const
{
	Vector ret;
	const Vector &n = m_normal;
	real_t fy = fabs(n.y), fz = fabs(n.z);
	if (fy < EPS && fz < EPS)
		ret.y = 1;
	else
	{
		ret.x = 1;
		if (fy > fz)
			ret.y = -n.x / n.y;
		else
			ret.z = -n.x / n.z;
		ret.normalize();
	}
	return ret;
}

// vim: syntax=cpp11.doxygen

