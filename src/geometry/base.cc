/*
 * $File: base.cc
 * $Date: Mon May 07 20:48:25 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geometry/base.hh"

#include <cassert>

bool PrimitiveBase::RayTask::get_tr_by_refractive_idx(Vector &ret,
		real_t n, const Vector &l_in, const Vector &normal)
{
	n = 1 / n;
	real_t cos1 = - normal.dot(l_in);
	if (cos1 < EPS)
		return false;
	assert(cos1 >= 0);
	real_t cos2 = 1 - n * n * (1 - cos1 * cos1);
	if (cos2 < 0)
		return false;
	cos2 = sqrt(cos2);

	ret = l_in * n + normal * (n * cos1 - cos2);
	return true;
}

// vim: syntax=cpp11.doxygen

