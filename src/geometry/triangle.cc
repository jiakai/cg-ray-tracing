/*
 * $File: triangle.cc
 * $Date: Thu Jun 07 20:31:09 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geometry/triangle.hh"

#include <cassert>


/*!
 * \brief 判断线段是否与矩形区域相交
 */
static bool test_intersect_box(const Vector2D &p0, const Vector2D &p1,
		real_t xmax, real_t ymax);

Triangle::Triangle(const Vector &v0, const Vector &v1, const Vector &v2):
	m_v0(v0), m_e1(v1, v0), m_e2(v2, v0),
	m_normal(m_e1.cross(m_e2)), m_norm_normal(m_normal.get_normalized())
{
}

bool Triangle::test_intersect(const BasicRay &ray, real_t &dist, real_t &u, real_t &v) const
{
	real_t detinv = ray.dir.dot(m_norm_normal);
	if (fabs(detinv) < EPS)
		return false;
	detinv = 1.0 / ray.dir.dot(m_normal);

	Vector sp0(ray.src, m_v0),
		   dxsp0(ray.dir.cross(sp0));
	u = dxsp0.dot(m_e2) * detinv;
	if (u < -EPS || u > 1 + EPS)
		return false;
	v = -dxsp0.dot(m_e1) * detinv;
	if (v < -EPS || u + v > 1 + EPS)
		return false;
	dist = sp0.dot(m_normal) * detinv;

	if (dist < -EPS)
		return false;

	assert((ray.get_point(dist) -(m_v0 * (1 - u - v) + get_v1() * u +
			get_v2() * v)).mod_sqr() < EPS_LOOSE);

	return true;
}

bool Triangle::test_intersect(const AABox &box) const
{
	static const Vector VEPS(EPS, EPS, EPS);
	Vector v[3] = {m_v0, get_v1(), get_v2()},
		boxdia(box.get_v0() - VEPS, box.get_v1() + VEPS);

	int nloose_inside = 0;
	for (int i = 0; i < 3; i ++)
	{
		if (box.test_inside(v[i]))
			return true;
		nloose_inside += box.test_inside_and_border(v[i]);
		for (int j = i + 1; j < 3; j ++)
			if (box.test_inside((v[i] + v[j]) * 0.5))
				return true;
	}
	if (nloose_inside == 3)
		return true;

	for (int i = 0; i < 3; i ++)
		v[i] -= box.get_v0() - VEPS;

	// test by setting each vertex of the triangle as source,
	// emitting two rays to other two vertices, intersecting
	// with each face of the box and to see whether the line
	// segment between the two intersection points intersects
	// with the box
	static const int OTHER_IDX[3][2] = {
		{1, 2}, {0, 2}, {0, 1}};
	for (int norm_comp = 0; norm_comp < 3; norm_comp ++)
		for (int dc = 0; dc < 2; dc ++)
		{
			real_t p0 = dc ? boxdia.get_comp(norm_comp) : 0;
			int nco1 = OTHER_IDX[norm_comp][0],
				nco2 = OTHER_IDX[norm_comp][1];

			for (int vsrc_idx = 0; vsrc_idx < 3; vsrc_idx ++)
			{
				const Vector &vsrc = v[vsrc_idx];

				Vector v1(vsrc, v[OTHER_IDX[vsrc_idx][0]]),
					   v2(vsrc, v[OTHER_IDX[vsrc_idx][1]]);
				real_t v1c = v1.get_comp(norm_comp), v2c = v2.get_comp(norm_comp),
					   vsc = vsrc.get_comp(norm_comp);
				if (fabs(v1c) > EPS && fabs(v2c) > EPS)
				{
					real_t t1 = (p0 - vsc) / v1c, t2 = (p0 - vsc) / v2c;
					if (t1 < EPS || t1 > 1 || t2 < EPS || t2 > 1)
						continue;
					Vector pt1(vsrc + v1 * t1),
						   pt2(vsrc + v2 * t2);
					if (test_intersect_box(
								Vector2D(pt1.get_comp(nco1), pt1.get_comp(nco2)),
								Vector2D(pt2.get_comp(nco1), pt2.get_comp(nco2)),
								boxdia.get_comp(nco1), boxdia.get_comp(nco2)))
							return true;
				}
			}
		}

	return false;
}

void Triangle::dump_mesh(MeshOutBase &mesh) const
{
	mesh.add_vtx(m_v0);
	mesh.add_vtx(get_v1());
	mesh.add_vtx(get_v2());
	mesh.begin_face();
	for (int i = 0; i < 3; i ++)
		mesh.add_face_vtx(i);
	mesh.end_face();
}

bool test_intersect_box(const Vector2D &p0, const Vector2D &p1,
		real_t xmax, real_t ymax)
{
#define IN(v, max) ((v >= -EPS) && (v) <= (max) + EPS)
	if ((IN(p0.x, xmax) && IN(p0.y, ymax)) ||
			(IN(p1.x, xmax) && IN(p1.y, ymax)))
		return true;
#undef IN

	auto ts = [&p0, &p1](const Vector2D &v0, const Vector2D &v1)
	{
		auto dts = [](const Vector2D &vs, const Vector2D &vt,
				const Vector2D &v0, const Vector2D &v1)
		{
			Vector2D d0(vs, v0), d1(vs, vt), d2(vs, v1);
			int f0 = get_sign(d0.cross(d1)), f1 = get_sign(d1.cross(d2));
			return f0 == f1 || !f0 || !f1;
		};

		return dts(p0, p1, v0, v1) && dts(v0, v1, p0, p1);
	};

	Vector2D v0(0, 0), v1(xmax, 0), v2(xmax, ymax), v3(0, ymax);

	return
		ts(v0, v1) || ts(v0, v3) ||
		ts(v2, v1) || ts(v0, v3);

}

// vim: syntax=cpp11.doxygen

