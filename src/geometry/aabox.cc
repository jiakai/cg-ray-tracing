/*
 * $File: aabox.cc
 * $Date: Thu May 03 20:08:05 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geometry/aabox.hh"

#include <limits>
#include <algorithm>
using namespace std;

AABox::AABox(const Vector &v0, const Vector &v1):
	m_vtx0(min(v0.x, v1.x), min(v0.y, v1.y), min(v0.z, v1.z)), 
	m_vtx1(max(v0.x, v1.x), max(v0.y, v1.y), max(v0.z, v1.z))
{}

bool AABox::get_intersection(const BasicRay &ray, real_t &dist, bool &inside) const
{
	real_t dfar = numeric_limits<real_t>::max(),
		   dnear = -dfar;

#define UPDATE(comp) \
	do \
	{ \
		if (fabs(ray.dir.comp) > EPS) \
		{ \
			real_t f = 1 / ray.dir.comp, \
				d0 = (m_vtx0.comp - ray.src.comp) * f, \
				d1 = (m_vtx1.comp - ray.src.comp) * f; \
			if (d0 > d1) \
				swap(d0, d1); \
			if (d0 > dnear) \
				dnear = d0; \
			if (d1 < dfar) \
				dfar = d1; \
		} \
	} while(0)

	UPDATE(x);
	UPDATE(y);
	UPDATE(z);

#undef UPDATE

	if (dfar < dnear + EPS || dfar < 0)
		return false;
	if (dnear < 0)
		dist = dfar, inside = true;
	else
		dist = dnear, inside = false;

	return true;
}

bool AABox::test_inside(const Vector &p) const
{
#define t(comp) (p.comp >= m_vtx0.comp + EPS && p.comp <= m_vtx1.comp - EPS)
	return t(x) && t(y) && t(z);
#undef t
}

bool AABox::test_inside_and_border(const Vector &p) const
{
#define t(comp) (p.comp >= m_vtx0.comp - EPS && p.comp <= m_vtx1.comp + EPS)
	return t(x) && t(y) && t(z);
#undef t
}

void AABox::dump_mesh(MeshOutBase &mesh) const
{
	static const int
		DCOMP[3][4] = {
			{1, 2, 6, 5},
			{2, 3, 7, 6},
			{4, 5, 6, 7}},
		FACE[6][4] = {
			{1, 2, 6, 5}, {0, 3, 7, 4},
			{2, 3, 7, 6}, {0, 4, 5, 1},
			{4, 5, 6, 7}, {0, 1, 2, 3}};

	for (int i = 0; i < 8; i ++)
	{
		Vector v(m_vtx0);
		for (int j = 0; j < 4; j ++)
			for (int c = 0; c < 3; c ++)
				if (DCOMP[c][j] == i)
					v.get_comp(c) = m_vtx1.get_comp(c);
		mesh.add_vtx(v);
	}

	for (int i = 0; i < 6; i ++)
	{
		mesh.begin_face();
		for (int j = 0; j < 4; j ++)
			mesh.add_face_vtx(FACE[i][j]);
		mesh.end_face();
	}
}

bool AABox::test_intersect(const AABox &box) const
{
	for (int i = 0; i < 3; i ++)
		if (m_vtx0.get_comp(i) > box.m_vtx1.get_comp(i) ||
				m_vtx1.get_comp(i) < box.m_vtx0.get_comp(i))
			return false;
	return true;
}

// vim: syntax=cpp11.doxygen

