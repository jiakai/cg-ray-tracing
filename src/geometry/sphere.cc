/*
 * $File: sphere.cc
 * $Date: Mon May 07 15:54:51 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geometry/sphere.hh"
#include "debug.hh"

class Sphere::RayTaskImpl: public Sphere::RayTask
{
	const Sphere &m_sphere;
	BasicRay m_ray;

	Vector m_src_ct; ///<! src 到球心的向量

	real_t m_proj,	///<! m_src_ct 在 m_dir 上投影的长度
		   m_r2md2;	///<! 半径平方减去球心到 m_dir 距离的平方

	int m_mask = 0;

	// intersection cache
	real_t m_intersection_dist = 0;
	Vector m_intersection_point;

	// normal vector cache
	Vector m_normal;

	static const int
		MASK_LS_INSIDE = 1,		// light source inside
		MASK_LS_ON_BORDER = 2,	// light source on border
		MASK_INTERSECTION_DONE = 4,
		MASK_NORMAL_DONE = 8;


	public:
		RayTaskImpl(const Sphere &sphere, const BasicRay &ray);

		bool test_intersect();

		real_t get_intersection();

		Vector get_normal();

		bool is_tangent()
		{ return (m_mask & (MASK_LS_ON_BORDER)) && fabs(m_proj) < EPS; }

		bool is_inside()
		{ return m_mask & MASK_LS_INSIDE; }

		bool is_on_border()
		{ return m_mask & MASK_LS_ON_BORDER; }

		const BasicRay& get_ray()
		{ return m_ray; }
};


Sphere::RayTaskImpl::RayTaskImpl(const Sphere &sphere, const BasicRay &ray):
	m_sphere(sphere), m_ray(ray), m_src_ct(ray.src, sphere.m_center)
{
}

bool Sphere::RayTaskImpl::test_intersect()
{
	m_proj = m_src_ct.dot(m_ray.dir);

	real_t src_ct_dist_sqr = m_src_ct.mod_sqr();

	if (src_ct_dist_sqr <= m_sphere.m_radius_sqr - EPS)
		m_mask |= MASK_LS_INSIDE;
	else
	{
		if (m_proj <= 0)
			return false;
		if (fabs(src_ct_dist_sqr - m_sphere.m_radius_sqr) < EPS)
			m_mask |= MASK_LS_ON_BORDER;
	}

	m_r2md2 = m_sphere.m_radius_sqr - m_src_ct.cross(m_ray.dir).mod_sqr();

	return m_r2md2 >= 0;
}

real_t Sphere::RayTaskImpl::get_intersection()
{
	if (m_mask & MASK_INTERSECTION_DONE)
		return m_intersection_dist;
	m_mask |= MASK_INTERSECTION_DONE;

	if (m_mask & MASK_LS_INSIDE)
		m_intersection_dist = m_proj + sqrt(m_r2md2);
	else if (m_mask & MASK_LS_ON_BORDER)
		m_intersection_dist = m_proj * 2;
	else
		m_intersection_dist = m_proj - sqrt(m_r2md2);

	assert(m_intersection_dist >= 0);

	m_intersection_point = m_ray.get_point(m_intersection_dist);

	if (fabs((m_intersection_point - m_sphere.m_center).mod_sqr() - m_sphere.m_radius_sqr) > EPS_LOOSE) 
	{
		print_debug("calculation error: act_r=%lf exp_r=%lf act_dis=%lf exp_dis=%lf",
				(m_intersection_point - m_sphere.m_center).mod_sqr(), m_sphere.m_radius,
				m_intersection_dist, (m_intersection_point - m_ray.src).mod());
		int m0 = m_mask ^ MASK_INTERSECTION_DONE;
		m_mask = 0;
		m_ray.src = m_ray.get_point(m_intersection_dist * 0.8);
		m_src_ct = m_sphere.m_center - m_ray.src;
		sassert(test_intersect());
		sassert(m_mask == m0);
		return get_intersection();
	}

	return m_intersection_dist;
}

Vector Sphere::RayTaskImpl::get_normal()
{
	if (m_mask & MASK_NORMAL_DONE)
		return m_normal;
	m_mask |= MASK_NORMAL_DONE;

	get_intersection();
	if (m_mask & (MASK_LS_INSIDE | MASK_LS_ON_BORDER))
		return m_normal = (m_sphere.m_center - m_intersection_point).get_normalized();
	return m_normal = (m_intersection_point - m_sphere.m_center).get_normalized();
}

Sphere::Sphere(const Vector &center, real_t radius):
	m_center(center), m_radius(radius), m_radius_sqr(radius * radius)
{
}

Sphere::RayTask* Sphere::make_ray_task(const BasicRay &ray) const
{
	Sphere::RayTaskImpl tmp(*this, ray);
	if (tmp.test_intersect())
		return new Sphere::RayTaskImpl(tmp);
	return nullptr;
}

bool Sphere::test_intersect(const BasicRay &ray) const
{
	Sphere::RayTaskImpl rt(*this, ray);
	return rt.test_intersect();
}

AABox Sphere::get_bounding_box() const
{
	Vector dr(m_radius, m_radius, m_radius);
	return AABox(m_center - dr, m_center + dr);
}

// vim: syntax=cpp11.doxygen
