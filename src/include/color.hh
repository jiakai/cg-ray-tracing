/*
 * $File: color.hh
 * $Date: Sat May 05 22:23:46 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 Color 类
 */

#ifndef _HEADER_COLOR_
#define _HEADER_COLOR_

#include "utils.hh"

#include <cmath>

/*!
 * \brief 描述单个颜色
 *
 * 注意：r, g, b分量的允许范围为[0, 1]
 */
struct Color
{
	typedef float component_t;	///!< 颜色分量所用实数类型
	static constexpr component_t EPS = 1e-3; ///!< 颜色比较时的允许误差

	component_t r, g, b;


	explicit Color(component_t r_ = 0, component_t g_ = 0, component_t b_ = 0) :
		r(r_), g(g_), b(b_)
	{}


	Color operator + (const Color &c) const
	{ return Color(r + c.r, g + c.g, b + c.b); }

	Color& operator += (const Color &c)
	{ r += c.r; g += c.g; b += c.b; return *this; }

	Color operator - (const Color &c) const
	{ return Color(r - c.r, g - c.g, b - c.b); }

	Color operator * (component_t f) const
	{ return Color(r * f, g * f, b * f); }

	Color operator * (const Color &c) const
	{ return Color(r * c.r, g * c.g, b * c.b); }

	Color& operator *= (const Color &c)
	{ r *= c.r; g *= c.g; b *= c.b; return *this; }

	Color& operator *= (component_t f)
	{ r *= f; g *= f; b *= f; return *this; }

	Color operator / (component_t f) const
	{ return *this * (1.0 / f); }

	component_t mod() const
	{ return sqrt(r * r + g * g + b * b); }

	bool is_black() const
	{ return fabs(r) < EPS && fabs(g) < EPS && fabs(b) < EPS; }

	/*!
	 * \brief 将大于1的颜色分量按比例置为1
	 */
	void set_to_range()
	{
		component_t max = r > g ? r : g;
		if (b > max) max = b;
		if (max > 1)
		{
			max = 1 / max;
			r *= max; g *= max; b *= max;
		}
	}

	void check_range() const
	{ sassert(r >= 0 && r <= 1); sassert(g >= 0 && g <= 1); sassert(b >= 0 && b <= 1); }

	static const Color
		BLACK, WHITE;
};

#endif // _HEADER_COLOR_

// vim: syntax=cpp11.doxygen

