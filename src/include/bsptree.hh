/*
 * $File: bsptree.hh
 * $Date: Mon May 07 10:05:52 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 BSPTree
 */

#ifndef _HEADER_BSPTREE_
#define _HEADER_BSPTREE_

#include "geometry/aabox.hh"
#include "geometry/base.hh"

/*!
 * \brief binary space partitioning tree
 */
class BSPTree
{
	public:
		struct Config
		{
			int max_depth,		///<! 划分树的最大深度
				inode_max_size;	///<! 未分割的内节点的最大大小
			real_t min_box_size;	///<! 包围盒最短边长度的最小值

			/*!
			 * \brief 某种经验上较优的配置
			 * \param npr 要加入的对象数量
			 */
			static Config make_optimal(int npr, real_t min_box_size = 1e-3);

			Config(int max_depth_, int inode_max_size_, real_t min_box_size_ = 1e-3):
				max_depth(max_depth_), inode_max_size(inode_max_size_),
				min_box_size(min_box_size_)
			{
			}
		};

		/*!
		 * \param box 指定划分树对应的空间区域
		 */
		BSPTree(const AABox &box, const Config &conf);
		~BSPTree();

		/*!
		 * \brief 被加入该划分树的对象
		 */
		class Primitive;

		void add_primitive(const RefCntPtr<Primitive>& primitive);

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray);

		const AABox& get_box() const;

	private:

		int m_prmtv_user_data_len = 0;
		Config m_conf;

		struct Node;
		Node *m_root;

		void do_add_primitive(Node *root, const RefCntPtr<Primitive>& primitive, int depth);

		struct StatData;

		PrimitiveBase::RayTask*
			do_make_ray_task(Node *root, const Ray &ray);

		BSPTree(const BSPTree&) = delete;
		BSPTree& operator = (const BSPTree&) = delete;
};

class BSPTree::Primitive
{
	public:
		/*!
		 * \brief 返回该对象是否与给定的AABox所围成的区域相交
		 */
		virtual bool test_intersect(const AABox &box) const = 0;

		/*!
		 * \brief 返回与求交相关的所需数据的大小
		 */
		virtual int get_user_data_size() const = 0;

		/*!
		 * \brief 求交，将求RayTask所需的数据存入user_data中。返回是否相交
		 */
		virtual bool get_intersection(const Ray &ray, real_t &dist,
				void *user_data) const = 0;

		/*!
		 * \brief 根据之前得到的 user_data 求 RayTask ，同时应清理 user_data
		 */
		virtual PrimitiveBase::RayTask* make_ray_task(const Ray &ray, void *user_data) const = 0;

		/*!
		 * \brief 进行相关的清理操作。
		 *
		 * 注：仅当 get_intersection 返回true，而最终没有选用该交点时调用本函数
		 */
		virtual void free_user_data(void *user_data) const = 0;

		virtual ~Primitive() {}
};

#endif // _HEADER_BSPTREE_

// vim: syntax=cpp11.doxygen

