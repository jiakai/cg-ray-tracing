/*
 * $File: mesh_impl.hh
 * $Date: Fri Jun 08 00:22:21 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 实现 MeshPrimitive 用到的一些内部结构
 */

#ifndef _HEADER_PRIMITIVE_MESH_IMPL_
#define _HEADER_PRIMITIVE_MESH_IMPL_

#include "primitives/mesh.hh"
#include "geometry/triangle.hh"
#include "bsptree.hh"

#include <vector>
#include <list>

namespace MeshPrimitiveImpl
{
	struct Face;
	struct Vertex;

	struct VertexExtraData
	{
		std::vector<Face*> adj_face;
		std::vector<Vertex*> adj_vtx;
		Vector2D velocity;
		std::size_t idx; ///<! index of this vertex in DataList::vtx
	};

	struct Vertex
	{
		Vector coord, normal;
		Vector2D map_coord;

		VertexExtraData *ext = nullptr;

		Vertex(const Vector &c):
			coord(c)
		{}
	};

	struct FaceExtraData
	{
		bool visited;
		std::list<Face*> adj_face;
	};

	struct Face
	{
		Triangle triangle;

		Vector normal; ///!< 如果 is_volume 被设定，则指向物体外面

		Vertex* const vtx[3];

		FaceExtraData *ext = nullptr;

		Face(Vertex &v0, Vertex &v1, Vertex &v2):
			triangle(v0.coord, v1.coord, v2.coord),
			normal(triangle.get_normal_unnormalized().get_normalized()),
			vtx{&v0, &v1, &v2}
		{
		}

		real_t get_area() const
		{ return triangle.get_normal_unnormalized().mod() * 0.5; }

		void add_adj_face(Face *f)
		{ ext->adj_face.push_back(f); }

		Vector get_norm_by_vtx_num(int vsrc, int v0, int v1) const
		{ return Vector(vtx[vsrc]->coord, vtx[v0]->coord).cross(
				Vector(vtx[vsrc]->coord, vtx[v1]->coord)); }
	};

}

struct MeshPrimitive::DataList
{
	std::vector<MeshPrimitiveImpl::Vertex> vtx;
	std::vector<MeshPrimitiveImpl::Face> face;

	std::vector<MeshPrimitiveImpl::VertexExtraData> vtx_ext;
	std::vector<MeshPrimitiveImpl::FaceExtraData> face_ext;

	BSPTree* bsp_tree = nullptr;

	~DataList()
	{ delete bsp_tree; }

	/*!
	* \brief 初始化vtx_ext, face_ext, adj_face 和 adj_vtx 等。
	 *
	 * 在调用该类的其它成员函数前，必须先调用本函数。
	 *
	 * \param check_volume 是否用检查图形的立体性，并输出错误信息。
	 */
	void init(bool check_volume); 

	/*!
	 * \brief 更改各面法向量的方向，保证其朝向一致
	 *
	 * \param test_dir 是否调用 regulate_single_face_norm 计算初始方向
	 */
	void regulate_face_norm(bool test_dir);

	/*!
	 * \brief 在 is_volume 设定了的情况下，将该面的法向量方向设定为朝外
	 */
	void regulate_single_face_norm(MeshPrimitiveImpl::Face &f0);

	/*!
	 * \brief 计算各点映射到平面上的坐标，用于纹理映射。
	 */
	void calc_vtx_map_coord();

	/*!
	 * \brief 初始化操作完成后，调用该函数清除无用的数据
	 */
	void clear_extra_data();
};


#endif // _HEADER_PRIMITIVE_MESH_IMPL_

// vim: syntax=cpp11.doxygen

