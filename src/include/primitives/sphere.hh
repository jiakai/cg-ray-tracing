/*
 * $File: sphere.hh
 * $Date: Thu May 03 20:43:15 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义球体
 */

#ifndef _HEADER_PRIMITIVES_SPHERE_
#define _HEADER_PRIMITIVES_SPHERE_

#include "geometry/base.hh"
#include "geometry/sphere.hh"
#include "lib/refcntptr.hh"
#include "bsptree.hh"

/*!
 * \brief 球体
 *
 * 注：纹理映射的值域为[0, pi * 2] * [0, pi]，直接由球面坐标转化而来
 */
class SpherePrimitive: public PrimitiveBase
{
	Sphere m_sphere;

	class RayTask;

	friend class RayTask;

	public:

		/*!
		 * \param sphere 对应的球
		 * \param opt_prpt 描述该球的光学性质
		 */
		SpherePrimitive(const Sphere &sphere, const RefCntPtr<PrimitiveBase::Material> &material);

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray) const;

		bool test_intersect(const AABox &box) const;

		void update_bounder(Vector &vmin, Vector &vmax) const;
};

#endif // _HEADER_PRIMITIVES_SPHERE_

// vim: syntax=cpp11.doxygen

