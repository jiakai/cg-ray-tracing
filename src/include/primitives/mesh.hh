/*
 * $File: mesh.hh
 * $Date: Fri May 04 15:51:27 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义三角网格类
 */

#ifndef _HEADER_PRIMITIVE_MESH_
#define _HEADER_PRIMITIVE_MESH_

#include "geometry/base.hh"
#include "bsptree.hh"

/*!
 * \brief 三角网格类
 *
 * 注：必须在顶点添加完后开始添加面
 */
class MeshPrimitive: public PrimitiveBase
{
	public:

		/*!
		 * 注：必须对封闭多面体才能开启is_volume；如果关闭本选项，则认为光源不会在图形内部
		 *
		 * 如果允许透射光，必须开启 is_volume
		 */
		struct Config
		{
			bool is_volume = false, ///<! 是否计算每个面的指向图形外部的法向量
				 smooth = false,	///<! 设置是否启用 smooth shading
				 auto_map_coord = false;	///<! 是否自动计算纹理映射的坐标
		};

		MeshPrimitive(const RefCntPtr<Material> &material, const Config &conf);
		~MeshPrimitive();

		/*!
		 * \brief 添加一个顶点
		 *
		 * 顶点按照添加顺序从0开始编号
		 */
		void add_vtx(const Vector &p);

		/*!
		 * \brief 设置某顶点纹理映射后的坐标
		 *
		 * \param v 顶点编号
		 * \param p 纹理映射的坐标
		 */
		void set_vtx_coord_map(int v, const Vector2D &p);

		/*!
		 * \brief 添加一个三角面片
		 *
		 * v0, v1, v2为对应顶点的编号
		 */
		void add_face(int v0, int v1, int v2);

		/*!
		 * \brief 完成初始化过程；所有顶点和面都添加完后调用本函数
		 */
		void finish_init();

		bool test_intersect(const AABox &box) const;
		void update_bounder(Vector &vmin, Vector &vmax) const;

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray) const;

	private:
		class RayTask;
		friend class RayTask;

		class BSPTreePrimitive;
		friend class BSPTreePrimitive;

		struct DataList;
		DataList *m_data_list;

		Config m_conf;

		MeshPrimitive(const MeshPrimitive&) = delete;
		MeshPrimitive& operator = (const MeshPrimitive&) = delete;
};

#endif // _HEADER_PRIMITIVE_MESH_

// vim: syntax=cpp11.doxygen

