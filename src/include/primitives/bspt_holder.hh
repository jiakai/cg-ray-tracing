/*
 * $File: bspt_holder.hh
 * $Date: Thu May 03 20:41:24 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 BSPTHolderPrimitive
 */

#ifndef _HEADER_PRIMITIVE_BSPT_HOLDER_
#define _HEADER_PRIMITIVE_BSPT_HOLDER_

#include "bsptree.hh"
#include "geometry/base.hh"

/*!
 * \brief 构造一个包含给定对象的 BSPTree
 */
class BSPTHolderPrimitive: public PrimitiveBase
{
	struct DataList;
	DataList *m_data_list;

	BSPTree* m_tree;

	class BSPTreePrimitive;
	friend class BSPTreePrimitive;

	BSPTree::Config m_bspt_conf;

	public:
		BSPTHolderPrimitive(const BSPTree::Config &conf);

		~BSPTHolderPrimitive();

		void add_primitive(const RefCntPtr<PrimitiveBase> &prmtv);

		/*!
		 * \brief 所有 BSPTree 添加完成后调用本函数
		 */
		void finish_add();


		PrimitiveBase::RayTask* make_ray_task(const Ray &ray) const;

		bool test_intersect(const AABox &box) const;

		void update_bounder(Vector &vmin, Vector &vmax) const;
};


#endif // _HEADER_PRIMITIVE_BSPT_HOLDER_

// vim: syntax=cpp11.doxygen

