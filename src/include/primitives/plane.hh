/*
 * $File: plane.hh
 * $Date: Fri May 04 11:41:20 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义平面类
 */

#ifndef _HEADER_PRIMITIVES_PLANE_
#define _HEADER_PRIMITIVES_PLANE_

#include "geometry/plane.hh"
#include "geometry/base.hh"
#include "utils.hh"

/*!
 * \brief 描述无限大的平面
 *
 * 关于纹理映射时的默认原点及方向选择，详见代码
 * 材质的折射系数被忽略
 */
class PlanePrimitive: public PrimitiveBase
{
	Plane m_plane;

	Vector m_p0, m_dir0, m_dir1;
		// a point on the plane and two directions parallel to the plane,
		// for computing texture coordinates

	class RayTask;

	friend class PlanePrimitive::RayTask;

	void find_p0();
	void find_dir0();

	public:

		/*!
		 * \param txt_p0 指定材质的原点；该点必须在平面上
		 * \param txt_dir0 指定材质的正方向；必须跟平面平行 
		 */
		PlanePrimitive(const Plane &plane, const RefCntPtr<Material> &material,
				const Vector *txt_p0 = nullptr, const Vector *txt_dir0 = nullptr);

		PrimitiveBase::RayTask* make_ray_task(const Ray &ray) const;

		bool test_intersect(const AABox &box) const
		{ error_exit("unsupported: PlanePrimitive::test_intersect"); }

		void update_bounder(Vector &vmin, Vector &vmax) const
		{ error_exit("unsupported: PlanePrimitive::update_bounder"); }
};

#endif // _HEADER_PRIMITIVES_PLANE_

// vim: syntax=cpp11.doxygen

