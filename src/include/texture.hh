/*
 * $File: texture.hh
 * $Date: Thu May 03 19:53:10 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义纹理基类
 */

#ifndef _HEADER_TEXTURE_
#define _HEADER_TEXTURE_

#include "mathbase.hh"

#include <functional>

/*!
 * \brief 描述物体在某点处的光学性质
 */
struct OpticalProperty
{
	Color diffuse = Color::BLACK;	///!< 漫反射系数
	real_t shininess = 0,		///!< 反射指数
		   specular = 0,		///!< 镜面反射系数
		   transparency = 0,	///!< 透明度，0为不透光，1为完全透明
		   ambient = 0;			///!< 环境光反射系数
};



/*!
 * \brief 简单二维纹理
 */
class TextureBase
{
	public:
		/*!
		 * \brief 取得某点处的光学性质
		 */
		virtual void get_opt_prpt(real_t x, real_t y,
				AutoPtr<const OpticalProperty> &ptr) const = 0;

		typedef std::function<void(real_t&, real_t&)> coord_trans_func_t;

		/*!
		 * \brief 设置进行坐标变换的函数
		 */
		void set_coordinate_transformer(const coord_trans_func_t &func)
		{ m_transform_coordinate = func; }

		virtual ~TextureBase() {}

	protected:
		 coord_trans_func_t m_transform_coordinate; ///!< 在实现 get_opt_prpt 时应该先调用本函数进行坐标变换

};

#endif // _HEADER_TEXTURE_

// vim: syntax=cpp11.doxygen
