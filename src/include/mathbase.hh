/*
 * $File: mathbase.hh
 * $Date: Thu Jun 07 16:23:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 基本数学类型定义及向量
 */

#ifndef _HEADER_MATHBASE_
#define _HEADER_MATHBASE_

#include "color.hh"

#include <cmath>
#include <cassert>

typedef double real_t; ///!< 图形计算时所用实数类型

const real_t
	EPS = 1e-6,			///!< 计算时的判0小量
	EPS_LOOSE = 1e-4;	///!< 进行结果校验时的判0小量

/*!
 * \brief 用val更新dest的最小值，返回是否成功
 */
template<typename T>
bool update_min(T &dest, const T &val)
{ if (val < dest) {dest = val; return true;} return false; }

/*!
 * \brief 用val更新dest的最大值，返回是否成功
 */
template<typename T>
bool update_max(T &dest, const T &val)
{ if (dest < val) {dest = val; return true;} return false; }

/*!
 * \brief 返回 x * x
 */
static inline real_t sqr(real_t x)
{ return x * x; }

/*!
 * \brief 返回-1表示x<0，返回0表示x==0，返回1表示x>0
 */
static inline int get_sign(real_t x)
{
	if (fabs(x) < EPS)
		return 0;
	return x < 0 ? -1 : 1;
}

/*!
 * \brief 三维向量，也用于对三维空间中点的表示
 */
struct Vector
{
	real_t x, y, z;

	explicit Vector(real_t x_ = 0, real_t y_ = 0, real_t z_ = 0):
		x(x_), y(y_), z(z_)
	{}

	/*!
	 * \brief p0到p1的向量
	 */
	Vector(const Vector &p0, const Vector &p1):
		x(p1.x - p0.x), y(p1.y -p0.y), z(p1.z - p0.z)
	{}

	/*!
	 * \brief 根据参数值返回对应的分量(0对应x，1对应y，其余为z)
	 */
	real_t get_comp(int c) const
	{ return c == 0 ? x : c == 1 ? y : z; }

	/*!
	 * \brief 根据参数值返回对应的分量(0对应x，1对应y，其余为z)
	 */
	real_t& get_comp(int c)
	{ return c == 0 ? x : c == 1 ? y : z; }

	/*!
	 * \brief 返回x,y,z分量的绝对值的最小值
	 */
	real_t get_min_comp_abs() const
	{ real_t a = fabs(x), b = fabs(y), c = fabs(z);
	 ::update_min(a, b); ::update_min(a, c); return a; }

	/*!
	 * \brief 向量长度的平方 
	 */
	real_t mod_sqr() const
	{ return x * x + y * y + z * z; }

	/*!
	 * \brief 向量长度
	 */
	real_t mod() const
	{ return sqrt(mod_sqr()); }

	/*!
	 * \brief 向量点积
	 */
	real_t dot(const Vector &v) const
	{ return x * v.x + y * v.y + z * v.z; }

	/*!
	 * \brief 向量叉积
	 */
	Vector cross(const Vector &v) const
	{ return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }


	Vector operator + (const Vector &v) const
	{ return Vector(x + v.x, y + v.y, z + v.z); }

	Vector& operator += (const Vector &v)
	{ x += v.x; y += v.y; z += v.z; return *this; }

	Vector operator - (const Vector &v) const
	{ return Vector(x - v.x, y - v.y, z - v.z); }

	Vector operator - () const
	{ return Vector(-x, -y, -z); }

	Vector& operator -= (const Vector &v)
	{ x -= v.x; y -= v.y; z -= v.z; return *this; }


	Vector operator * (real_t f) const
	{ return Vector(x * f, y * f, z * f); }

	Vector& operator *= (real_t f)
	{ x *= f; y *= f; z *= f; return *this; }

	/*!
	 * \brief 以向量为法向量，求v的反射向量
	 *
	 * 注：要求本向量和v都是单位向量
	 */
	Vector get_reflection(const Vector &v) const
	{ assert(fabs(mod_sqr() - 1) < EPS && fabs(v.mod_sqr() - 1) < EPS);
		return *this * (dot(v) * 2) - v; }

	/*!
	 * \brief 将本向量单位化
	 */
	void normalize()
	{
		real_t m = 1 / mod(); x *= m; y *= m; z *= m;
		assert(std::isnormal(m));
		m = mod();
		if (fabs(m - 1) > EPS)
		{
			m = 1 / mod();
			x *= m; y *= m; z *= m;
		}
	}

	/*!
	 * \brief 返回单位化后的向量
	 */
	Vector get_normalized() const
	{ Vector ret(*this); ret.normalize(); return ret; }

	/*!
	 * \brief 判断是否为零向量
	 */
	bool is_zero() const
	{ return fabs(x) < EPS && fabs(y) < EPS && fabs(z) < EPS; }

	/*!
	 * \brief 用v中的各分量来向减小方向更新本向量的各分量
	 */
	void update_min(const Vector &v)
	{ ::update_min(x, v.x); ::update_min(y, v.y); ::update_min(z, v.z); }

	/*!
	 * \brief 用v中的各分量来向增大方向更新本向量的各分量
	 */
	void update_max(const Vector &v)
	{ ::update_max(x, v.x); ::update_max(y, v.y); ::update_max(z, v.z); }

	bool operator == (const Vector &v) const
	{ return fabs(x - v.x) < EPS && fabs(y - v.y) < EPS && fabs(z - v.z) < EPS; }
};


/*!
 * \brief 平面向量
 */
struct Vector2D
{
	real_t x, y;

	explicit Vector2D(real_t x_ = 0, real_t y_ = 0):
		x(x_), y(y_)
	{}

	/*!
	 * \brief p0到p1的向量
	 */
	Vector2D(const Vector2D &p0, const Vector2D &p1):
		x(p1.x - p0.x), y(p1.y -p0.y)
	{}

	real_t cross(const Vector2D &v) const
	{ return x * v.y - y * v.x; }

	real_t dot(const Vector2D &v) const
	{ return x * v.x + y * v.y; }

	Vector2D operator + (const Vector2D &v) const
	{ return Vector2D(x + v.x, y + v.y); }

	Vector2D& operator += (const Vector2D &v)
	{ x += v.x; y += v.y; return *this; }

	Vector2D operator - (const Vector2D &v) const
	{ return Vector2D(x - v.x, y - v.y); }

	Vector2D& operator -= (const Vector2D &v)
	{ x -= v.x; y -= v.y; return *this; }

	Vector2D operator * (real_t f) const
	{ return Vector2D(x * f, y * f); }

	bool is_zero() const
	{ return fabs(x) < EPS && fabs(y) < EPS; }

	real_t mod_sqr() const
	{ return x * x + y * y; }

	real_t mod() const
	{ return sqrt(mod_sqr()); }

	Vector2D get_normalized() const
	{ real_t m = mod(); assert(m > EPS); m = 1 / m; return Vector2D(x * m, y * m); }

	void set_zero() 
	{ x = y = 0; }
};


/*!
 * \brief 描述光线
 */
struct BasicRay
{
	Vector src,	///!< 光源位置
		   dir;	///!< 光线方向，单位向量

	BasicRay(const Vector &src_, const Vector &dir_):
		src(src_), dir(dir_)
	{}

	BasicRay() {}

	/*!
	 * \brief 取光线上到光源距离为给定值的点
	 */
	Vector get_point(real_t dist) const
	{ return Vector(src.x + dir.x * dist, src.y + dir.y * dist, src.z + dir.z * dist); }

};

#endif // _HEADER_MATHBASE_

// vim: syntax=cpp11.doxygen

