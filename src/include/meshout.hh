/*
 * $File: meshout.hh
 * $Date: Wed May 02 22:25:07 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 MeshOutBase
 */

#ifndef _HEADER_MESHOUT_
#define _HEADER_MESHOUT_

#include "mathbase.hh"

/*!
 * \brief 三角网格输出的基类
 */
class MeshOutBase
{
	public:
		/*!
		 * \brief 添加一个点
		 */
		virtual void add_vtx(const Vector &p) = 0;

		/*!
		 * \brief 添加一个新的面
		 */
		virtual void begin_face() = 0;

		/*!
		 * \brief 向当前面中添加一个顶点(顶点编号从0开始)
		 */
		virtual void add_face_vtx(int v) = 0;

		/*!
		 * \brief 该面顶点添加完毕
		 */
		virtual void end_face() = 0;

		/*!
		 * \brief 添加一个线段作为单独的一个面 (helper function)
		 */
		virtual void add_line_segment(const Vector &v0, const Vector &v1)
		{
			add_vtx(v0); add_vtx(v1);
			begin_face(); add_face_vtx(0); add_face_vtx(1); end_face();
		}

		virtual ~MeshOutBase() {}
};

#endif // _HEADER_MESHOUT_

// vim: syntax=cpp11.doxygen

