/*
 * $File: mkcamera.hh
 * $Date: Fri May 04 16:36:22 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 mkcamera
 */

#ifndef _HEADER_MKCAMERA_
#define _HEADER_MKCAMERA_

#include "camera.hh"

/*!
 * \brief 返回所需的Camera实例。需要调用 delete 释放内存
 */
Camera* mkcamera();

#endif // _HEADER_MKCAMERA_

// vim: syntax=cpp11.doxygen

