/*
 * $File: debug.hh
 * $Date: Sat May 05 20:08:51 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 调试用的相关函数
 */

#ifndef _HEADER_DEBUG_
#define _HEADER_DEBUG_


#ifdef CONFIG_DEBUG

#include "meshout.hh"
/*!
 * \brief 向调试用的obj文件中加入一个Mesh
 *
 * 注：调用者负责进行delete
 */
MeshOutBase* new_debug_mesh_out(const char *name);


static inline void debug_mesh_out_line_segment(const char *name, const Vector &v0, const Vector &v1)
{
	auto p = new_debug_mesh_out(name);
	p->add_line_segment(v0, v1);
	delete p;
}


/*!
 * \brief 输出调试信息
 *
 * 仅当 CONFIG_DEBUG 被定义时会输出调试信息
 */
#define print_debug(fmt, ...) \
			__print_debug__(__FILE__, __func__, __LINE__, fmt, ## __VA_ARGS__)


void __print_debug__(const char *file, const char *func, int line, const char *fmt, ...)
	__attribute__((format(printf, 4, 5)));


#else // CONFIG_DEBUG


#define print_debug(fmt, ...)


#endif // CONFIG_DEBUG


#endif  // _HEADER_DEBUG_

// vim: syntax=cpp11.doxygen

