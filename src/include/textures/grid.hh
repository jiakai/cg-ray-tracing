/*
 * $File: grid.hh
 * $Date: Mon Apr 23 13:51:01 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 GridTexture
 */

#ifndef _HEADER_TEXTURE_GRID_
#define _HEADER_TEXTURE_GRID_

#include "texture.hh"

/*!
 * \brief 方格纹理
 */
class GridTexture: public TextureBase
{
	OpticalProperty
		m_prpt_space,
		m_prpt_line;
	real_t m_grid_width, m_grid_height,
		   m_border_width;

	public:

		GridTexture(const OpticalProperty &prpt_space,
				const OpticalProperty &prpt_line,
				real_t grid_width = 1, real_t grid_height = 1, real_t border_width = 0.05):
			m_prpt_space(prpt_space), m_prpt_line(prpt_line),
			m_grid_width(grid_width), m_grid_height(grid_height), m_border_width(border_width)
		{}

		void get_opt_prpt(real_t x, real_t y,
				AutoPtr<const OpticalProperty> &ptr) const;
};

#endif // _HEADER_TEXTURE_GRID_

// vim: syntax=cpp11.doxygen

