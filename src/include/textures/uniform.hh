/*
 * $File: uniform.hh
 * $Date: Sun Apr 15 10:24:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 UniformTexture
 */

#ifndef _HEADER_TEXTURE_UNIFORM_
#define _HEADER_TEXTURE_UNIFORM_

#include "texture.hh"

/*!
 * \brief 处处统一的材质
 */
class UniformTexture: public TextureBase
{
	OpticalProperty m_prpt;

	public:
		UniformTexture(const OpticalProperty &prpt):
			m_prpt(prpt)
		{}


		void get_opt_prpt(real_t, real_t,
				AutoPtr<const OpticalProperty> &ptr) const
		{ ptr.set(&m_prpt, false); }
};

#endif // _HEADER_TEXTURE_UNIFORM_

// vim: syntax=cpp11.doxygen

