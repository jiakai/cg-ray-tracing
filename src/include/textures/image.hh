/*
 * $File: image.hh
 * $Date: Fri May 04 18:21:56 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 ImageTexture 类
 */

#ifndef _HEADER_TEXTURE_IMAGE_
#define _HEADER_TEXTURE_IMAGE_

#include "texture.hh"

/*!
 * \brief 贴图纹理
 */
class ImageTexture: public TextureBase
{
	int m_width, m_height;
	Color *m_pixels;

	OpticalProperty m_base_opt_prpt;

	real_t m_x_scale, m_y_scale;

	ImageTexture(const ImageTexture &) { sassert(0); }
	void operator = (const ImageTexture &) { sassert(0); }

	public:
		
		/*!
		 * \param base_opt_prpt 提供除了漫反射外的其它参数。
		 * \param x_scale x的缩放系数，详见 get_opt_prpt
		 * \param y_scale y的缩放系数，详见 get_opt_prpt
		 */
		ImageTexture(const char *fpath, const OpticalProperty &base_opt_prpt,
				real_t x_scale = 1, real_t y_scale = 1);
		~ImageTexture();


		/*!
		 * 注：坐标变换流程为： x *= x_scale; y *= y_scale; transform_coordinate(x, y); x %= 1; y %= 1;
		 */
		void get_opt_prpt(real_t x, real_t y,
				AutoPtr<const OpticalProperty> &ptr) const;
};

#endif // _HEADER_TEXTURE_IMAGE_

// vim: syntax=cpp11.doxygen

