/*
 * $File: utils.hh
 * $Date: Fri May 04 11:39:35 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 常用的一些小工具
 */

#ifndef _HEADER_UTILS_
#define _HEADER_UTILS_

#include <sys/time.h>
#include <cstddef>


/*!
 * \brief 向一个静态数组里面写出字符串
 */
char* ssprintf(const char *fmt, ...) __attribute__((format(printf, 1, 2)));

/*!
 * \brief 输出错误信息，终止程序
 */
void error_exit(const char *msg) __attribute__((noreturn));

/*!
 * \brief solid assert
 *
 * 保证表达式一定会恰好被执行一次，
 * 如果返回值为false则报错退出。
 */
#define sassert(expr) \
	__sassert_check__((expr), # expr, __FILE__, __PRETTY_FUNCTION__, __LINE__)


/*!
 * \brief 基于硬件时钟的计时器
 */
class HWTimer
{
	timeval m_start;

	public:
		
		HWTimer();

		double get_sec() const;
};

/*!
 * \brief 可以设定对象销毁时是否释放内存的指针类型
 *
 * 比 RefCntPtr 更弱，应用范围也更窄，但效率更高；非线程安全。
 */
template<typename T>
class AutoPtr
{
	T *m_ptr;
	bool m_del;

	void free()
	{ if (m_del && m_ptr) delete m_ptr; }

	public:
		AutoPtr(const AutoPtr &) = delete;
		AutoPtr& operator = (const AutoPtr &) = delete;

		AutoPtr(T *ptr = nullptr, bool del = false):
			m_ptr(ptr), m_del(del)
		{}

		~AutoPtr()
		{ free(); }

		void set(T *ptr, bool del)
		{ free(); m_ptr = ptr; m_del = del; }

		T* get_ptr() const { return m_ptr; }

		T* operator -> () const { return get_ptr(); }
		T& operator *() const { return *get_ptr(); }

		operator bool() const
		{ return m_ptr != nullptr; }
};

void __sassert_check__(bool val, const char *expr,
		const char *file, const char *func, int line);


#endif // _HEADER_UTILS_

// vim: syntax=cpp11.doxygen

