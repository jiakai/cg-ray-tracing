/*
 * $File: plane.hh
 * $Date: Thu May 03 20:08:38 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 Plane 类
 */

#ifndef _HEADER_GEOMETRY_PLANE_
#define _HEADER_GEOMETRY_PLANE_

#include "mathbase.hh"

/*!
 * \brief 平面
 */
class Plane
{
	Vector m_normal;
	real_t m_c;

	public:
		/*!
		 * \param normal 平面法向量所在方向
		 * \param c 决定平面位置的常量：点x在平面上当且仅当normal.dot(x) == c
		 */
		Plane(const Vector &normal, real_t c):
			m_normal(normal.get_normalized()), m_c(c / normal.mod())
		{ sassert(!normal.is_zero()); }

		/*!
		 * \param normal 平面法向量所在方向
		 * \param p 在平面上的一点
		 */
		Plane(const Vector &normal, const Vector &p):
			m_normal(normal.get_normalized()), m_c(m_normal.dot(p))
		{ sassert(!normal.is_zero()); }

		/*!
		 * \brief 计算光源沿光线方向到平面的距离，结果存入dist中
		 *
		 * 返回false表示光线与平面平行
		 */
		bool get_intersection(const BasicRay &ray, real_t &dist) const
		{
			real_t b = m_normal.dot(ray.dir);
			if (fabs(b) < EPS)
				return false;
			dist = (m_c -  m_normal.dot(ray.src)) / b;
			return true;
		}

		/*!
		 * \brief 返回一点到平面的距离
		 */
		real_t get_dist(const Vector &p) const
		{ return m_c - m_normal.dot(p); }

		/*!
		 * \brief 计算某点按法方向在平面上的投影
		 */
		Vector get_projection(const Vector &p) const
		{ return p + m_normal * (m_c - m_normal.dot(p)); }

		/*!
		 * \brief 返回平面法向量
		 */
		const Vector& get_normal() const
		{ return m_normal; }

		/*!
		 * \brief 返回平移常数
		 */
		real_t get_c() const
		{ return m_c; }

		/*!
		 * \brief 测试某点是否在平面上
		 */
		bool test_on_plane(const Vector &p) const
		{ return fabs(m_normal.dot(p) - m_c) < EPS; }

		/*!
		 * \brief 测试某向量是否跟该平面平行
		 */
		bool test_parallel(const Vector &v) const
		{ return fabs(m_normal.dot(v)) < EPS; }

		bool operator == (const Plane &p) const
		{ return (m_normal == p.m_normal && fabs(m_c - p.m_c) < EPS) ||
			(-m_normal == p.m_normal && fabs(m_c + p.m_c) < EPS); }

		/*!
		 * \brief 返回平面内的某点
		 */
		Vector find_point() const;

		/*!
		 * \brief 返回跟平面平行的一个向量
		 */
		Vector find_vector() const;
};

#endif // _HEADER_GEOMETRY_PLANE_

// vim: syntax=cpp11.doxygen

