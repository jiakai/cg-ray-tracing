/*
 * $File: aabox.hh
 * $Date: Thu May 03 20:07:52 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 AABox 类
 */

#ifndef _HEADER_GEOMETRY_AABOX_
#define _HEADER_GEOMETRY_AABOX_

#include "meshout.hh"
#include "mathbase.hh"

/*!
 * \brief axel-aligned box
 */
class AABox
{
	Vector m_vtx0, m_vtx1;

	public:
		
		/*!
		 * \param vtx0 立方体的某顶点
		 * \param vtx1 与vtx0构成体对角线端点的另一顶点
		 */
		AABox(const Vector &vtx0, const Vector &vtx1);

		/*!
		 * \brief 求交。返回是否相交，若相交则交点到光源距离存入dist中。
		 */
		bool get_intersection(const BasicRay &ray, real_t &dist, bool &inside) const;

		/*!
		 * \brief 测试某点是否在该立方体内部（严格内部）
		 */
		bool test_inside(const Vector &p) const;

		/*!
		 * \brief 测试某点是否在该立方体内部或边界上
		 */
		bool test_inside_and_border(const Vector &p) const;

		/*!
		 * \brief 测试是否与另一个 AABox 相交
		 */
		bool test_intersect(const AABox &box) const;

		/*!
		 * \brief 返回x,y,z坐标的最小值
		 */
		const Vector& get_v0() const
		{ return m_vtx0; }

		/*!
		 * \brief 返回x,y,z坐标的最大值
		 */
		const Vector& get_v1() const
		{ return m_vtx1; }

		/*!
		 * \brief 将该图形输出至 mesh 中
		 */
		void dump_mesh(MeshOutBase &mesh) const;
};


#endif // _HEADER_GEOMETRY_AABOX_

// vim: syntax=cpp11.doxygen

