/*
 * $File: base.hh
 * $Date: Mon May 07 15:52:11 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义几何对象及包围体的基类
 */

#ifndef _HEADER_GEOMETRY_BASE_
#define _HEADER_GEOMETRY_BASE_

#include "color.hh"
#include "mathbase.hh"
#include "lib/refcntptr.hh"
#include "texture.hh"

class PrimitiveBase;
class AABox;

/*!
 * \brief 有扩展功能的光线
 */
struct Ray: public BasicRay
{
	Ray(const Vector &src, const Vector &dir):
		BasicRay(src, dir)
	{}

	Ray() {}

	typedef bool (*test_visible_func_t)(const Ray &ray, const PrimitiveBase&);



	/*!
	 * \brief 判断是否为对该光线“可见”的物体
	 *
	 * 在实现 PrimitiveBase::make_ray_task 的底层，应该调用本函数进行判断
	 */
	test_visible_func_t test_visible = _default_test_visible;

	Ray& operator = (const BasicRay &r)
	{ src = r.src; dir = r.dir; return *this; }

	private:

		static bool _default_test_visible(const Ray &ray, const PrimitiveBase &)
		{ return true; }

};

/*!
 * \brief 可见的三维几何对象基类 
 */
class PrimitiveBase
{
	public:

		/*!
		 * \brief 描述物体的材质
		 */
		class Material;

		/*!
		 * \brief 提供与某条特定光线相关联的一些计算实现
		 *
		 * 注：物体是否反光、透光的性质是通过其 OpticalProperty 里
		 * 对应的系数是否为0来表示。
		 */
		class RayTask;

		PrimitiveBase(const RefCntPtr<Material> &mat):
			m_material(mat)
		{}

		virtual ~PrimitiveBase() {}
	

		/*!
		 * \brief 创建一个跟指定光线相关的 PrimitiveBase::RayTask 实例
		 *
		 * 这样设计的意义在于，尽可能让几何对象在判断是否相交时产生的
		 * 结果用于辅助计算交点、反射光方向等。
		 *
		 * 实现时应注意线程安全。
		 *
		 * 返回nullptr表示不相交。
		 *
		 * ** 调用者需要负责释放内存 **
		 */
		virtual RayTask* make_ray_task(const Ray &ray) const = 0;


		/*!
		 * \brief 返回该对象是否与 box 围成的区域相交
		 */
		virtual bool test_intersect(const AABox &box) const = 0;

		/*!
		 * \brief 更新包围体范围
		 * 
		 * \param vmin 各分量的最小值
		 * \param vmax 各分量的最大值
		 */
		virtual void update_bounder(Vector &vmin, Vector &vmax) const = 0;

		const Material& get_material() const
		{ return *m_material.get_ptr(); }

	protected:
		RefCntPtr<Material> m_material;
};

class PrimitiveBase::Material
{
	public:

		/*!
		 * \param refractive_idx 折射系数，即出射角与入射角正弦值之比
		 */
		Material(const RefCntPtr<TextureBase> &texture, real_t refractive_idx = 1.5):
			m_texture(texture), m_refractive_idx(refractive_idx)
		{}

		/*!
		 * \brief 定义光照模式的有关常量
		 */
		struct IllumMode
		{
			typedef int mask_t;
			static constexpr mask_t
				REFLECTION = 1,	///<! 是否反光
				SHADOW = 2;		///<! 是否在其它物体上透射阴影
		};

		/*!
		 * \brief 将光照模式设置为指定值
		 */
		void set_illum_mode(IllumMode::mask_t mode)
		{ m_illum_mode = mode; }

		/*!
		 * \brief 添加指定的光照模式选项
		 */
		void add_illum_mode(IllumMode::mask_t mode)
		{ m_illum_mode |= mode; }

		/*!
		 * \brief 移除指定的光照模式选项
		 */
		void rm_illum_mode(IllumMode::mask_t mode)
		{ m_illum_mode &= ~mode; }

		/*!
		 * \brief 返回光照模式
		 */
		IllumMode::mask_t get_illum_mode() const
		{ return m_illum_mode; }

		/*!
		 * \brief 返回折射系数
		 */
		real_t get_refractive_idx() const
		{ return m_refractive_idx; }

		/*!
		 * \brief 返回材质
		 */
		const TextureBase& get_texture() const
		{ return *m_texture.get_ptr(); }

	private:
		IllumMode::mask_t m_illum_mode = IllumMode::REFLECTION | IllumMode::SHADOW;
		RefCntPtr<TextureBase> m_texture;
		real_t m_refractive_idx;
};

class PrimitiveBase::RayTask
{
	protected:
		/*!
		 * \brief 通过折射率求折射光方向
		 *
		 * 返回是否发生折射(即没有发生全反射)
		 *
		 * \param l_in 入射光方向
		 * \param normal 法向量
		 */
		static bool get_tr_by_refractive_idx(Vector &ret,
				real_t refractive_idx, const Vector &l_in,
				const Vector &normal);

		const PrimitiveBase* m_primitive;

	public:

		RayTask(const PrimitiveBase* primitive):
			m_primitive(primitive)
		{}

		virtual ~RayTask() {}

		const PrimitiveBase& get_primitive() const
		{ return *m_primitive; }


		/*!
		 * \brief 求交点，返回交点到光源的距离
		 */
		virtual real_t get_intersection() = 0;

		/*!
		 * \brief 求交点处的单位法向量
		 */
		virtual Vector get_normal() = 0;

		/*!
		 * \brief 求透射光方向
		 *
		 * 注：如果发生全反射，则应返回全反射的出射光。
		 */
		virtual void get_transmission(Ray &ret) = 0;

		/*!
		 * \brief 求物体在交点处的光学性质
		 *
		 * 注：给出的光线方向是出射方向的反方向，
		 * 因此镜面反射系数、透射系数等应对应于相应的入射光到所给出的出射光的系数。
		 */
		virtual void get_opt_prpt(AutoPtr<const OpticalProperty> &ptr) = 0;
};

#include "geometry/aabox.hh"

#endif // _HEADER_GEOMETRY_BASE_

// vim: syntax=cpp11.doxygen

