/*
 * $File: sphere.hh
 * $Date: Mon May 07 13:57:30 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 Sphere 类
 */

#ifndef _HEADER_GEOMETRY_SPHERE_
#define _HEADER_GEOMETRY_SPHERE_

#include "geometry/aabox.hh"
#include "mathbase.hh"

/*!
 * \brief 球体
 */
class Sphere
{
	Vector m_center;
	real_t m_radius, m_radius_sqr;

	class RayTaskImpl;
	friend class Sphere::RayTaskImpl;

	public:

		Sphere(const Vector &center, real_t radius);

		class RayTask;

		/*!
		 * \brief 创建一个跟指定光线相关的 Sphere::RayTask 实例
		 *
		 * 返回nullptr表示不相交。
		 *
		 * ** 调用者需要负责释放内存 **
		 */
		RayTask* make_ray_task(const BasicRay &ray) const;

		/*!
		 * \brief 仅测试是否与指定光线相交
		 */
		bool test_intersect(const BasicRay &ray) const;

		const Vector& get_center() const
		{ return m_center; }

		real_t get_radius() const
		{ return m_radius; }

		void set_radius(real_t r)
		{ m_radius = r; m_radius_sqr = r * r; }

		AABox get_bounding_box() const;
};

class Sphere::RayTask
{
	public:
		/*!
		 * \brief 求交点，返回交点到光源的距离
		 */
		virtual real_t get_intersection() = 0;

		/*!
		 * \brief 求交点处的单位法向量
		 */
		virtual Vector get_normal() = 0;

		/*!
		 * \brief 返回该光线是否为球体的切线
		 */
		virtual bool is_tangent() = 0;

		/*!
		 * \brief 返回光源是否在球体内部
		 */
		virtual bool is_inside() = 0;

		/*!
		 * \brief 返回光源是否在球面上
		 */
		virtual bool is_on_border() = 0;

		/*!
		 * \brief 返回对应的光线
		 */
		virtual const BasicRay& get_ray() = 0;

		virtual ~RayTask() {}
};


#endif // _HEADER_GEOMETRY_SPHERE_

// vim: syntax=cpp11.doxygen

