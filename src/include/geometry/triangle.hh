/*
 * $File: triangle.hh
 * $Date: Thu Jun 07 18:56:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 Triangle
 */

#ifndef _HEADER_GEOMETRY_TRIANGLE_
#define _HEADER_GEOMETRY_TRIANGLE_

#include "meshout.hh"
#include "mathbase.hh"
#include "geometry/aabox.hh"

class Triangle
{
	Vector m_v0,
		   m_e1,		///!< v0 - v1
		   m_e2,		///!< v0 - v2
		   m_normal,	///!< unnomalized normal vector
		   m_norm_normal;	///<! normalized normal vector

	bool do_test_intersect(const Triangle &trngl) const;

	public:

		Triangle(const Vector &v0, const Vector &v1, const Vector &v2);

		Vector get_v0() const
		{ return m_v0; }
		Vector get_v1() const
		{ return m_v0 - m_e1; }
		Vector get_v2() const
		{ return m_v0 - m_e2; }

		/*!
		 * \brief 返回未单位化的法向量
		 */
		const Vector& get_normal_unnormalized() const
		{ return m_normal; }

		/*!
		 * \brief 返回光线是否与该三角形相交
		 *
		 * 结果满足：
		 * dist >= EPS, ray.get_point(dist) == v0 * (1 - u - v) + v1 * u + v2 * v
		 */
		bool test_intersect(const BasicRay &ray, real_t &dist, real_t &u, real_t &v) const;

		/*!
		 * \brief 测试三角形是否与AAbox围成的区域相交(使用宽松判定：box的区域向
		 *			周边扩展EPS)
		 */
		bool test_intersect(const AABox &box) const;

		/*!
		 * \brief 将该三角形输出至 mesh 中
		 */
		void dump_mesh(MeshOutBase &mesh) const;
};

#endif // _HEADER_GEOMETRY_TRIANGLE_

// vim: syntax=cpp11.doxygen

