/*
 * $File: scene.hh
 * $Date: Thu Jun 07 16:13:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义关于场景渲染的相关类
 */

#ifndef _HEADER_SCENE_
#define _HEADER_SCENE_

#include "geometry/base.hh"
#include "lib/refcntptr.hh"

/*!
 * \brief 用于描述一个场景
 */
class Scene
{
	struct DataList;
	DataList *m_data_list;

	double m_min_weight, m_ambient_factor;

	Color m_ambient;
	int m_depth_limit;

	struct StatData;
	StatData *m_stat;



	Color do_ray_tracing(const Ray &ray, double weight, int depth) const;

	PrimitiveBase::RayTask* find_intersection(const Ray &ray) const;

	bool test_shadow(const Ray &ray, real_t max_dist) const;

	Scene(const Scene&) = delete;
	Scene& operator = (const Scene &) = delete;

	public:
		/*!
		 * \param min_weight 最小光线权值，当光低于该值时停止跟踪
		 * \param ambient_factor 计算环境光所用的系数：环境光 = sum(光源i *
		 *		i.weight) * ambient_factor
		 *
		 * \param max_depth 递归最大深度
		 */
		Scene(double min_weight, double ambient_factor, int max_depth);


		~Scene();

		/*!
		 * \brief 修改最大递归深度
		 */
		void set_max_depth(int d)
		{ m_depth_limit = d; }

		/*!
		 * \brief 描述点光源
		 */
		struct PointLightSource
		{
			Vector pos;
			Color color;
			double weight;

			PointLightSource(const Vector &p, const Color &c, double w):
				pos(p), color(c), weight(w)
			{}
		};

		/*!
		 * \brief 向场景中加入一个几何对象
		 */
		void add_primitive(const RefCntPtr<PrimitiveBase> &ptr);

		/*!
		 * \brief 向场景中加入一个光源
		 *
		 * 注意：所有光源的权值和必须为1。
		 */
		void add_point_light_source(const PointLightSource &pls);

		/*!
		 * \brief 使用光线跟踪法渲染一个像素点
		 *
		 * 在调用该函数之前，必须先调用 ray_tracing_init
		 */
		Color ray_tracing(const Ray &ray) const;

		void ray_tracing_init();

		/*!
		 * \brief 返回最大递归深度
		 */
		int get_max_recursive_depth() const;

		/*!
		 * \brief 返回平均递归深度
		 */
		double get_avg_recursive_depth() const;
};


#endif // _HEADER_SCENE_

// vim: syntax=cpp11.doxygen

