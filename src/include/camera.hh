/*
 * $File: camera.hh
 * $Date: Mon May 07 10:24:39 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 Camera
 */
#ifndef _HEADER_CAMERA_
#define _HEADER_CAMERA_

#include "scene.hh"

class Camera
{
	public:
		struct Config
		{
			Vector
				view_point,	///<! 视点位置
				sensor_p0,	///<! 感光器件某角
				sensor_du,	///<! 感光器所在平面的一边
				sensor_dv;	///<! 感光器所在平面的另一边

			int dof_nray_min = 0,	///<! DOF光线的最小数目，设为比2小的值则禁用 DOF
				dof_nray_max = 40;	///<! DOF光线的最大数目
			real_t focal_dist,		///<! 焦平面到视点的距离
				   aperture,		///<! 光圈直径（数值越大，光圈越大）
				   dof_error = 0;	///<! 当新的光线对当前平均值改变量小于该值时，停止DOF。

			/*!
			 * \brief 设置该相机跟踪某点，要求 view_point 已设置好
			 *
			 * \param target 跟踪目标
			 * \param du_plane_norm	sensor_du 与以该向量为法向的平面平行，视线方向、该向量、sensor_du 构成右手系
			 * \param du_mod sensor_du 的模长
			 * \param dist 视点到感光器的距离
			 * \param ratio sensor_du 与 sensor_dv 的长度之比
			 */
			void trace_point(const Vector &target, const Vector &du_plane_norm,
					real_t du_mod, real_t dist, real_t ratio);

			/*!
			 * \brief 设置焦点，要求view_point 和 sensor 的已初始化
			 */
			void set_focus(const Vector &target);
		};

		Camera(const RefCntPtr<Scene> &scene, const Config &conf);
		~Camera();

		/*!
		 * \brief 计算某点的和颜色值
		 *
		 * u, v 均在[0, 1]范围内，表示 Config::sensor_du 和 Config::sensor_dv 的
		 * 比例
		 */
		Color get_pixel(real_t u, real_t v) const;

		Scene& get_scene() 
		{ return *m_scene.get_ptr(); }

		/*!
		 * \brief 返回单次DOF所用光线最大数量
		 */
		int get_max_dof_nray() const;

		/*!
		 * \brief 返回DOF所用光线平均数量
		 */
		double get_avg_dof_nray() const;

	private:
		RefCntPtr<Scene> m_scene;
		Config m_conf;
		Vector m_sensor_normal,
			   m_du_normal, m_dv_normal;

		struct StatData;
		StatData *m_stat;

		Vector get_rand_unit() const;
};

#endif // _HEADER_CAMERA_

// vim: syntax=cpp11.doxygen

