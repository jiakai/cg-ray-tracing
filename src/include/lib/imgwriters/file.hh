/*
 * $File: file.hh
 * $Date: Fri Apr 06 22:14:54 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 FileImgWriter ，用于写图像文件。
 */

#ifndef _HEADER_FILE_IMG_WRITER_
#define _HEADER_FILE_IMG_WRITER_

#include "lib/imgwriter.hh"

class _FileImgWriterImpl;

/*!
 * \brief 将图像保存为文件
 */
class FileImgWriter: public ImgWriter
{
	_FileImgWriterImpl *impl;

	protected:

		void write_impl(int x, int y, const Color &pixel);

	public:
		
		FileImgWriter(const Geometry &g, const char *fpath);
		~FileImgWriter();

		void finish();
};


#endif // _HEADER_FILE_IMG_WRITER_

// vim: syntax=cpp11.doxygen

