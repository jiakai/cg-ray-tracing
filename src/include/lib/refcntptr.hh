/*
 * $File: refcntptr.hh
 * $Date: Mon Jun 04 20:47:08 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 实现 RefCntPtr
 */

#ifndef _HEADER_REFCNTPTR_
#define _HEADER_REFCNTPTR_

/*!
 * \brief 基于引用计数的智能指针
 *
 * 当引用计数减为0时，自动析构对象。
 *
 * 注：不保证线程安全
 */
template<typename T>
class RefCntPtr
{
	struct Holder
	{
		T* ptr;
		int cnt;

		Holder(T* p):
			ptr(p), cnt(1)
		{}

		~Holder()
		{ delete ptr; ptr = nullptr; }
	};
	Holder *m_holder;

	void incr_ref()
	{  m_holder->cnt ++; }

	void decr_ref()
	{
		if (m_holder && !(-- m_holder->cnt))
		{
			delete m_holder;
			m_holder = nullptr;
		} 
	}

	public:
		RefCntPtr(T *p = nullptr):
			m_holder(new Holder(p))
		{}

		RefCntPtr(const RefCntPtr &r):
			m_holder(r.m_holder)
		{ incr_ref(); }

		~RefCntPtr()
		{ decr_ref(); }

		RefCntPtr& operator = (const RefCntPtr &r)
		{
			if (m_holder != r.m_holder)
			{
				decr_ref();
				m_holder = r.m_holder;
				incr_ref();
			}
			return *this;
		}

		T* get_ptr() const { return m_holder->ptr; }

		T* operator -> () const { return get_ptr(); }
		T& operator *() const { return *get_ptr(); }

		bool operator == (const RefCntPtr<T> &p) const
		{ return m_holder->ptr == p.m_holder->ptr; }

		bool operator == (T *p) const
		{ return m_holder->ptr == p; }

		operator bool() const
		{ return m_holder->ptr != nullptr; }
};

#endif // _HEADER_REFCNTPTR_

// vim: syntax=cpp11.doxygen

