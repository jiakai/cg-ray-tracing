/*
 * $File: thread.hh
 * $Date: Sat Apr 14 18:07:08 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 定义 thread::Thread 线程类。
 */

#ifndef _HEADER_THREAD_THREAD_
#define _HEADER_THREAD_THREAD_


#include <functional>
#include <pthread.h>

namespace thread
{
	/*!
	 * \brief 线程类，当实例被创建时即开始执行新线程
	 */
	class Thread
	{
		pthread_t m_id;

		public:
			typedef std::function<void()> func_t;

			Thread(const func_t &func);

			Thread(const Thread &t);

			Thread& operator = (const Thread &t);

			void join();

			static void wait_all();
	};
}

#endif // _HEADER_THREAD_THREAD_

// vim: syntax=cpp11.doxygen
