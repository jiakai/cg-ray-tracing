/*
 * $File: lock.hh
 * $Date: Sat Apr 14 17:27:48 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 提供线程锁的相关对象
 */

#ifndef _HEADER_THREAD_LOCK_
#define _HEADER_THREAD_LOCK_

#include <pthread.h>

#include "utils.hh"

namespace thread
{
	/*!
	 * \brief 互斥锁
	 */
	class MutexLock
	{
		pthread_mutex_t m_lock;

		public:
			MutexLock(const MutexLock &) = delete;
			void operator = (const MutexLock &) = delete;

			MutexLock();
			~MutexLock();
			void enter();
			void exit();
	};

	/*!
	 * \brief 对 MutexLock 的一种包装
	 *
	 * 对象实例被创建时，调用lock.enter()，
	 * 销毁时调用lock.exit()
	 */
	class ScopedMutexLock
	{
		MutexLock &m_lock;

		ScopedMutexLock(const ScopedMutexLock &t): m_lock(t.m_lock)
		{ sassert(0); }

		void operator = (const ScopedMutexLock &) { sassert(0); }

		public:
			ScopedMutexLock(MutexLock &lock);
			~ScopedMutexLock();
	};
}

#endif // _HEADER_THREAD_LOCK_

// vim: syntax=cpp11.doxygen
