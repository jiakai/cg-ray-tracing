/*
 * $File: prgrsbar.hh
 * $Date: Fri Apr 06 22:15:03 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 控制台渲染进度条
 */

#ifndef _HEADER_PRGRSBAR_
#define _HEADER_PRGRSBAR_

#include "lib/imgwriter.hh"

/*!
 * \brief 在控制台显示渲染进度条。
 *
 * 该函数会新开启一个线程来显示进度条并立即返回，
 * 因此不会阻塞调用者。
 */
void show_console_progressbar(const ImgWriter &writer);

#endif // _HEADER_PRGRSBAR_

// vim: syntax=cpp11.doxygen

