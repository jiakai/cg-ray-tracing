/*
 * $File: imgwriter.hh
 * $Date: Fri Apr 06 22:14:44 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 提供关于图像处理的基本工具
 */

#ifndef _HEADER_IMGWRITER_
#define _HEADER_IMGWRITER_

#include "lib/thread.hh"
#include "color.hh"


/*!
 * \brief 定义输出图像的抽象基类接口
 *
 * 在实现时，覆盖 ImgWriter::write_impl 即可，
 * 并可根据需要覆盖 ImgWriter::init 或 ImgWriter::finish 。
 * 另外，ImgWriter::write 实现了线程安全的封装。
 *
 * 注意：对于每个像素，应该写入恰好一次。
 *
 */
class ImgWriter
{
	public:
		/*!
		 * \brief 描述一个矩形对象，包含宽度和高度
		 */
		struct Geometry
		{
			int width, height;

			Geometry(int w = 0, int h = 0) :
				width(w), height(h)
			{}

			int get_area() const
			{ return width * height; }
		};

		ImgWriter(const Geometry &g);

		virtual ~ImgWriter() {}

		/*!
		 * \brief 在执行写入操作之前该函数会被调用。
		 */
		virtual void init() {}


		/*!
		 * \brief 在所有像素写完之后该函数会被调用。
		 */
		virtual void finish() {}

		/*!
		 * \brief 写入单个像素的颜色值。
		 *
		 * 注意：窗口左上角坐标为(0, 0)
		 */
		void write(int x, int y, const Color &pixel);

		const Geometry& get_geometry() const { return m_geometry; }

		/*!
		 * \brief 返回已经绘制的像素的数量。
		 */
		int get_pixel_cnt() const;

	private:
		mutable thread::MutexLock m_lock;
		int m_pxl_cnt;

		Geometry m_geometry;

	protected:
		virtual void write_impl(int x, int y, const Color &pixel) = 0;

};

#endif // _HEADER_IMGWRITER_

// vim: syntax=cpp11.doxygen
