/*
 * $File: thread.hh
 * $Date: Fri Apr 06 22:15:21 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 所有线程操作相关的函数、类等。
 */

#ifndef _HEADER_THREAD_
#define _HEADER_THREAD_

#include "lib/thread/lock.hh"
#include "lib/thread/thread.hh"

#endif

// vim: syntax=cpp11.doxygen
