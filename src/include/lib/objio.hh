/*
 * $File: objio.hh
 * $Date: Tue Jun 05 10:33:41 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 解析 Wavefront(.obj) 文件
 */

#ifndef _HEADER_OBJIO_
#define _HEADER_OBJIO_

#include "mathbase.hh"
#include "meshout.hh"
#include "geometry/base.hh"

#include <cstdio>
#include <vector>
#include <functional>

/*!
 * \brief 输出 .obj 文件
 */
class ObjOutput
{
	FILE *m_fout;
	bool m_close_on_dtor;
	int m_vtx_cnt = 0;

	class MeshOut;
	friend class MeshOut;

	public:

		ObjOutput(FILE *fout, bool close_on_dtor = true);
		~ObjOutput();

		/*!
		 * \brief 添加一个网格(调用者负责释放对象)
		 */
		MeshOutBase* add_mesh(const char *name);
};


/*!
 * \brief 解析 .obj 文件
 */
class ObjInput
{

	struct DataList;
	DataList *m_data_list;

	void parse_mtllib(const char *fpath);

	class FileReader;
	class MeshMaker;

	friend class MeshMaker;

	protected:
		virtual void on_new_primitive(const RefCntPtr<PrimitiveBase> &prmtv) = 0;

	public:

		void work(const char *fpath);
		virtual ~ObjInput() {}
};


#endif // _HEADER_OBJIO_

// vim: syntax=cpp11.doxygen

