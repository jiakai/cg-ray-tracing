/*
 * $File: streamio.hh
 * $Date: Fri May 04 13:39:09 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief 提供对各种类的流输入输出的重载
 */

#ifndef _HEADER_STREAMIO_
#define _HEADER_STREAMIO_

#include "mathbase.hh"
#include "color.hh"

#include <iostream>
#include <sstream>
#include <string>

static inline std::ostream& operator << (std::ostream &ostr, const Vector &v)
{
	return ostr << "{" << v.x << ", " << v.y << ", " << v.z << "}";
}

static inline std::ostream& operator << (std::ostream &ostr, const Color &c)
{
	return ostr << "color{" << c.r << ", " << c.g << ", " << c.b << "}";
}

static inline std::istream& operator >> (std::istream &istr, Vector &v)
{
	return istr >> v.x >> v.y >> v.z;
}

static inline std::istream& operator >> (std::istream &istr, Color &c)
{
	return istr >> c.r >> c.g >> c.b;
}

template <typename T>
std::string tostr(const T &val)
{
	std::ostringstream ostr;
	ostr << val;
	return ostr.str();
}

#endif // _HEADER_STREAMIO_

// vim: syntax=cpp11.doxygen

