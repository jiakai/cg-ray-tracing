/*
 * $File: main.cc
 * $Date: Fri Jun 08 00:06:56 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "mkcamera.hh"

#include "lib/thread.hh"
#include "lib/imgwriters/file.hh"
#include "lib/prgrsbar.hh"

#include "debug.hh"

#include <cstdio>
#include <cstdlib>
#include <cstring>

struct ThreadConf
{
	const Camera *camera;
	ImgWriter *fout;
	int x0, x1, y0, y1, img_width, img_height;
};
static void working_thread(ThreadConf conf);
static const char* _get_conf(const char *name, const char *dflt);
#define get_conf(n, d) strdupa(_get_conf(n, d))

#define print_msg(...) \
	do \
	{ \
		if (!Conf::QUIET) \
			printf(__VA_ARGS__); \
	} while(0)

int main()
{
	Conf::QUIET = getenv("QUIET") != NULL;
	print_msg("Jiakai's Naive Ray Tracing Engine started ...\n");
	const char *fout_path = get_conf("FOUT", "output/test.jpg");
	int nthread = atoi(get_conf("NTHREAD", "1"));
	sassert(nthread > 0);
	int img_size = atoi(get_conf("IMG_SIZE", "640"));
	sassert(img_size > 0);
	sassert((Conf::IMAGE_RATIO_W = atoi(get_conf("IMG_RATIO_W", "3"))) > 0);
	sassert((Conf::IMAGE_RATIO_H = atoi(get_conf("IMG_RATIO_H", "2"))) > 0);
	Conf::IMAGE_RATIO = (double)Conf::IMAGE_RATIO_W / Conf::IMAGE_RATIO_H;

	ThreadConf thconf;
	thconf.img_width = img_size * Conf::IMAGE_RATIO_W;
	thconf.img_height = img_size * Conf::IMAGE_RATIO_H;
	assert(thconf.img_width > 0 && thconf.img_height > 0);

	print_msg("image width=%d height=%d\n", thconf.img_width, thconf.img_height);
	FileImgWriter fout(ImgWriter::Geometry(thconf.img_width, thconf.img_height), fout_path);
	fout.init();

	Camera *cam = mkcamera();

	thconf.camera = cam;
	thconf.fout = &fout;
	thconf.y0 = 0;
	thconf.y1 = thconf.img_height;
	thconf.x0 = 0;

	show_console_progressbar(fout);
	for (int i = 0; i < nthread; i ++)
	{
		thconf.x1 = thconf.img_width * (i + 1) / nthread;
		thread::Thread th(std::bind(&working_thread, thconf));
		thconf.x0 = thconf.x1;
	}

	thread::Thread::wait_all();

	print_debug("max: recursive_depth=%d   dof_nray=%d",
			cam->get_scene().get_max_recursive_depth(),
			cam->get_max_dof_nray());
	print_debug("avg: recursive_depth=%.3lf dof_nray=%.3lf",
			cam->get_scene().get_avg_recursive_depth(),
			cam->get_avg_dof_nray());

	delete cam;

	fout.finish();

}

const char* _get_conf(const char *name, const char *dflt)
{
	char *str = getenv(name);
	if (!str)
	{
		print_msg("environment variable %s not set, use %s as default value.\n",
				name, dflt);
		return dflt;
	}
	print_msg("%s=%s\n", name, str);
	return str;
}

void working_thread(ThreadConf conf)
{
	for (int x = conf.x0; x < conf.x1; x ++)
		for (int y = conf.y0; y < conf.y1; y ++)
			conf.fout->write(x, y, conf.camera->get_pixel(
						real_t(x) / conf.img_width,
						real_t(y) / conf.img_height));
}

namespace Conf
{
	int IMAGE_RATIO_W, IMAGE_RATIO_H;
	double IMAGE_RATIO;
	bool QUIET;
}

// vim: syntax=cpp11.doxygen

