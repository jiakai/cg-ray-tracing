# $File: Makefile

VIEW ?= gqview output/test.jpg
NTHREAD ?= 2

BUILD_DIR = build
TARGET = ./ray-tracing

CONFIG_FILE = src/config.hh

override OPTFLAG ?= -O3
override CPPFLAGS += -std=c++11 -include $(CONFIG_FILE) -Isrc/include \
	$(shell Magick++-config --cppflags)
override CXXFLAGS += $(OPTFLAG) -Wall -Wextra -Wnon-virtual-dtor -Wno-unused-parameter \
	-pthread -ggdb $(CPPFLAGS) \
	$(shell Magick++-config --cxxflags)
override LDFLAGS += $(OPTFLAG) -pthread -ggdb \
	$(shell Magick++-config --ldflags --libs)

CXX = g++

CXXSOURCES = $(shell find src -name "*.cc")
OBJS = $(addprefix $(BUILD_DIR)/,$(CXXSOURCES:.cc=.o))
DEPFILES = $(OBJS:.o=.d)


all: $(TARGET)

$(BUILD_DIR)/%.o: %.cc
	@echo "[cxx] $< ..."
	@$(CXX) -c $< -o $@ $(CXXFLAGS)

$(BUILD_DIR)/%.d: %.cc
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(CPPFLAGS) -MM -MT "$@ $(@:.d=.o)" "$<"  > "$@"

sinclude $(DEPFILES)

$(TARGET): $(OBJS)
	@echo "Linking ..."
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS)
	@echo -e "CXXFLAGS='$(CXXFLAGS)'\nLDFLAGS='$(LDFLAGS)'"
	@echo -e "\033[1;34mBuiding of JNRTE done\033[0m"


clean:
	rm -rf $(BUILD_DIR) $(TARGET) gmon.out

run: $(TARGET)
	@NTHREAD=$(NTHREAD) $(TARGET)
	@$(VIEW)

gdb: $(TARGET)
	@NTHREAD=1 gdb $(TARGET)

git:
	git add -A
	git commit -a

valgrind: $(TARGET)
	NTHREAD=$(NTHREAD) valgrind --leak-check=full --leak-check=yes \
		--log-file=output/mem.log --tool=memcheck \
		$(TARGET)
	cat output/mem.log

.PHONY: all clean run git gdb valgrind
