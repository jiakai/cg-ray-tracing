#!/bin/bash -e
# $File: mkavi_list.sh
# $Date: Mon May 07 19:04:18 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

DIR=../output/anim
echo > list
f=0
while [ $f -lt $(ls $DIR/*.jpg | wc -l) ]
do
	echo "$DIR/$f.jpg" >> list
	let f=$f+1
done

