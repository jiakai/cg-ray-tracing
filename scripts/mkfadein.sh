#!/bin/bash -e
# $File: mkfadein.sh
# $Date: Mon May 07 19:30:53 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

num=0
d=100
while [ $d -ge 0 ]
do
	FOUT=../output/anim-fade-in/$num.jpg
	echo $FOUT
	composite -dissolve $d ../output/title.jpg ../output/anim/0.jpg $FOUT
	let d=$d-4
	let num=$num+1
done

