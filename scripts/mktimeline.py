#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: mktimeline.py
# $Date: Sun Jun 10 08:51:50 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

#output: <frame number> <arguments to engine>
def time_cube(t, s):
    # integrate(x*(s-x), x, 0, s) and scale to 1
    f = lambda x: 0.5 * s * x ** 2 - 1.0 / 3 * x ** 3
    return f(t) / f(s)

def time_linear(t, s):
    return float(t) / s

TIME = [1.3, 0.7, 0.5, 0.5, 0.5, 3]
FUNC = [time_cube] * 5 + [time_linear]
FPS = 50


def get_arg(time):
    p = 0
    while time >= TIME[p]:
        time -= TIME[p]
        p += 1
    return "3 {0} {1}".format(p, FUNC[p](time, TIME[p]))

if __name__ == '__main__':
    for i in range(int(sum(TIME) * FPS)):
        print i, get_arg(float(i) / FPS)

