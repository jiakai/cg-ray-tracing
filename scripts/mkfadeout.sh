#!/bin/bash -e
# $File: mkfadeout.sh
# $Date: Mon May 07 21:52:16 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

num=0
d=100
FLAST=../output/anim/$(( $(ls ../output/anim | wc -l) - 1 )).jpg
while [ $d -ge 0 ]
do
	FOUT=../output/anim-fade-out/$num.jpg
	echo $FOUT
	composite -dissolve $d $FLAST black.jpg $FOUT
	let d=$d-4
	let num=$num+1
done

