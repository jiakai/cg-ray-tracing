#!/bin/bash -e
# $File: mkavi.sh
# $Date: Mon May 07 19:04:07 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

mencoder mf://@list -mf fps=25:type=jpg \
	-ovc x264 -x264encopts threads=2 -o ../output/output.avi

