#!/bin/bash -e
# $File: mkanim.sh
# $Date: Mon May 07 14:52:11 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

cd ..
export IMG_SIZE=360
export NTHREAD=2
while read frame arg
do
	echo "Frame #$frame"
	echo $arg | FOUT=output/anim/$frame.jpg ./ray-tracing
done | tee mkanim.log

